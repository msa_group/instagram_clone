package com.instagram.clone.like;

import com.instagram.clone.common.security.JwtAuthentication;
import com.instagram.clone.like.dto.request.CommentLikeRequest;
import com.instagram.clone.like.dto.request.PostLikeRequest;
import com.instagram.clone.like.service.LikeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@DataJpaTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureMockMvc
public class LikeTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LikeService likeService;

    @Test
    public void commentLikeTest(){
    }

    @Test
    public void postLikeTest(){

    }
}

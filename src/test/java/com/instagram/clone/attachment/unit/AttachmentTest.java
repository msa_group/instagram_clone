package com.instagram.clone.attachment.unit;

import com.instagram.clone.attachment.service.AttachmentService;
import com.instagram.clone.common.error.UnprocessableEntityException;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import javax.transaction.Transactional;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;
import java.io.IOException;

@SpringBootTest
public class AttachmentTest {

    @Autowired
    private AttachmentService attachmentService;

    @Test
    @DisplayName("Post Photo with JPG file")
    @Transactional
    void testNewNormalPostPhotoJpg(){
        Resource resource = new ClassPathResource("attachment/mock-image.jpg");

        try {
            FileUtils.readFileToByteArray(resource.getFile());
            MultipartFile multipartFile = new MockMultipartFile(
                    "file",
                    "test.jpg",
                    "image/jpeg",
                    FileUtils.readFileToByteArray(resource.getFile())
            );
            attachmentService.savePostPhoto(multipartFile);
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    @DisplayName("Post Photo with JPEG file")
    @Transactional
    void testNewNormalPostPhotoJpeg(){
        Resource resource = new ClassPathResource("attachment/mock-image-jpeg.jpeg");

        try {
            FileUtils.readFileToByteArray(resource.getFile());
            MultipartFile multipartFile = new MockMultipartFile(
                    "file",
                    "test.jpeg",
                    "image/jpeg",
                    FileUtils.readFileToByteArray(resource.getFile())
            );
            attachmentService.savePostPhoto(multipartFile);
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    @DisplayName("Post Photo with PNG file")
    @Transactional
    void testNewNormalPostPhotoPng(){
        Resource resource = new ClassPathResource("attachment/mock-image.png");

        try {
            FileUtils.readFileToByteArray(resource.getFile());
            MultipartFile multipartFile = new MockMultipartFile(
                    "file",
                    "test.png",
                    "image/png",
                    FileUtils.readFileToByteArray(resource.getFile())
            );
            attachmentService.savePostPhoto(multipartFile);
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    @DisplayName("Post Photo with text file with jpg extension")
    @Transactional
    void testNewNormalPostPhotoWithFakeJpg(){
        Resource resource = new ClassPathResource("attachment/mock-image-fake.jpg");
        assertThrows(UnprocessableEntityException.class, ()->{
            FileUtils.readFileToByteArray(resource.getFile());
            MultipartFile multipartFile = new MockMultipartFile(
                    "file",
                    "test.jpeg",
                    "image/jpeg",
                    FileUtils.readFileToByteArray(resource.getFile())
            );
            attachmentService.savePostPhoto(multipartFile);
        });
    }

    @Test
    @DisplayName("Post Photo with WEBP")
    @Transactional
    void testNewNormalPostPhotoWithWebp(){
        Resource resource = new ClassPathResource("attachment/mock-image.webp");
        assertThrows(UnprocessableEntityException.class, ()->{
            FileUtils.readFileToByteArray(resource.getFile());
            MultipartFile multipartFile = new MockMultipartFile(
                    "file",
                    "test.webp",
                    "image/webp",
                    FileUtils.readFileToByteArray(resource.getFile())
            );
            attachmentService.savePostPhoto(multipartFile);
        });
    }
}

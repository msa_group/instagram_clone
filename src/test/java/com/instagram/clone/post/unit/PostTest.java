package com.instagram.clone.post.unit;


import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.account.repository.AccountRepository;
import com.instagram.clone.attachment.model.entity.Attachment;
import com.instagram.clone.attachment.repository.AttachmentRepository;
import com.instagram.clone.common.error.BadRequestException;
import com.instagram.clone.common.error.ForbiddenException;
import com.instagram.clone.common.error.NotFoundException;
import com.instagram.clone.common.security.JWT;
import com.instagram.clone.common.security.JwtAuthentication;
import com.instagram.clone.post.model.dto.request.EditPostRequestDTO;
import com.instagram.clone.post.model.dto.request.NewPostRequestDTO;
import com.instagram.clone.post.model.dto.response.EditPostResponseDTO;
import com.instagram.clone.post.model.dto.response.NewPostResponseDTO;
import com.instagram.clone.post.model.entity.Post;
import com.instagram.clone.post.repository.PostRepository;
import com.instagram.clone.post.service.PostService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
public class PostTest {

    @Autowired
    private PostService postService;

    private Post dummyPost;
    private Account dummyAccount;
    private JwtAuthentication dummyAuth;
    private Attachment dummyAttachment;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AttachmentRepository attachmentRepository;

    @Autowired
    private PostRepository postRepository;

    @BeforeEach
    void setupEach(){
        Account account = Account.builder()
                .username("whateveruser")
                .nickname("testnick")
                .password("plaintext")
                .build();
        dummyAccount = accountRepository.save(account);

        dummyPost = Post.builder()
                .author(dummyAccount)
                .content("Dummy post")
                .postDt(Instant.now())
                .updateDt(Instant.now())
                .build();
        postRepository.save(dummyPost);


        dummyAuth = new JwtAuthentication(
                dummyAccount.getId(),
                dummyAccount.getUsername(),
                dummyAccount.getNickname()
        );


        Attachment postPhoto = Attachment.builder()
                .uri("https://localhost:8888/post-photo/test-photo.png")
                .uploadDt(Instant.now())
                .attachmentType(Attachment.AttachmentType.POST_PHOTO)
                .build();
        dummyAttachment = attachmentRepository.save(postPhoto);
    }

    @Test
    @DisplayName("Make new post with invalid media ID")
    void testNewPostWithInvalidMediaID(){
        NewPostRequestDTO newPostReq = NewPostRequestDTO
                .builder()
                .postContent("")
                .mediaID(-1L)
                .build();

        assertThrows(
                NotFoundException.class,
                () -> postService.makeNewPost(newPostReq, dummyAccount.getUsername())
        );
    }

    @Test
    @DisplayName("Normal new post")
    @Transactional
    void testNormalNewPost() throws Exception {
        Attachment postPhoto = Attachment.builder()
                .uri("https://localhost:8888/post-photo/test-photo.png")
                .uploadDt(Instant.now())
                .attachmentType(Attachment.AttachmentType.POST_PHOTO)
                .build();
        Attachment newPhoto = attachmentRepository.save(postPhoto);

        NewPostRequestDTO newPostReq = NewPostRequestDTO
                .builder()
                .postContent("neW pOST")
                .mediaID(newPhoto.getId())
                .build();
        Account testAccount = Account.builder()
                .username("testuser")
                .password("testpass")
                .nickname("testnicks")
                .build();
        accountRepository.save(testAccount);

        NewPostResponseDTO newPostRes = postService.makeNewPost(newPostReq, testAccount.getUsername());

        Post foundPost = postRepository.findById(newPostRes.getId()).orElseThrow(Exception::new);
        assertEquals(newPostRes.getId(), foundPost.getId());
    }


    @Test
    @DisplayName("New Post with empty content")
    public void testEmptyNewPost(){
        NewPostRequestDTO newPostPayload = NewPostRequestDTO
                .builder()
                .mediaID(dummyAttachment.getId())
                .postContent(" ")
                .build();

        assertThrows(BadRequestException.class, ()-> postService.makeNewPost(
                newPostPayload, dummyAuth.getUserName()));
    }


    @Test
    @DisplayName("Edit Post")
    public void testEditPost(){
        EditPostRequestDTO editPayload = EditPostRequestDTO
                .builder()
                .postContent("Hello edited post")
                .build();

        JwtAuthentication auth = new JwtAuthentication(
                dummyAccount.getId(),
                dummyAccount.getUsername(),
                dummyAccount.getNickname()
        );

        EditPostResponseDTO editedPostDTO = postService.editPost(dummyPost.getId(), editPayload, auth);
        assertEquals("Hello edited post", editedPostDTO.getContent());
        assertEquals(dummyPost.getId(), editedPostDTO.getId());
    }

    @Test
    @DisplayName("Edit Post with empty content")
    public void testEmptyEdit(){
        EditPostRequestDTO editPayload = EditPostRequestDTO
                .builder()
                .postContent(" ")
                .build();

        assertThrows(BadRequestException.class, ()-> postService.editPost(dummyPost.getId(), editPayload, dummyAuth));
    }

    @Test
    @DisplayName("Edit someone else's post")
    public void testEditAnotherPersonsPost(){
        JwtAuthentication anotherAuth = new JwtAuthentication(
                999L, "hackeruser", "hackernick"
        );

        EditPostRequestDTO editPayload = EditPostRequestDTO
                .builder()
                .postContent("wow new hack")
                .build();

        assertThrows(ForbiddenException.class, () -> postService.editPost(dummyPost.getId(), editPayload, anotherAuth));
    }

    @Test
    @DisplayName("Delete post")
    public void testDeletePost(){

        postService.deletePost(dummyPost.getId(), dummyAuth);
        Post deletedPost = postRepository.findById(dummyPost.getId()).get();
        assertEquals(dummyPost.getId(), deletedPost.getId());
        assertTrue(deletedPost.isDeleted());
    }

    @Test
    @DisplayName("Delete post with unauthorized user")
    public void testDeleteAnotherPersonsPost(){
        JwtAuthentication anotherAuth = new JwtAuthentication(
                999L, "hackeruser", "hackernick"
        );

        assertThrows(ForbiddenException.class, () -> postService.deletePost(dummyPost.getId(), anotherAuth));
    }



}


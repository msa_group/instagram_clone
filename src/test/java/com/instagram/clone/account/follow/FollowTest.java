package com.instagram.clone.account.follow;

import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.common.security.JWT;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static com.instagram.clone.common.security.model.Role.USER;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureMockMvc
public class FollowTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JWT jwt;

    private String _1_accountToken;
    private String _2_accountToken;
    private String _3_accountToken;
    private String _27_accountToken;


    @BeforeAll
    void setUp() {
        Account _1_account = Account.builder()
                .id(1)
                .username("남동길")
                .nickname("Giri")
                .password("123123")
                .build();
        Account _2_account = Account.builder()
                .id(2)
                .username("팔로워1")
                .nickname("_2_account")
                .password("123123")
                .build();
        Account _3_account = Account.builder()
                .id(3)
                .username("팔로워2")
                .nickname("_3_account")
                .password("123123")
                .build();
        Account _27_account = Account.builder()
                .id(27)
                .username("팔로워27")
                .nickname("_27_account")
                .password("123123")
                .build();
        JWT.Claims _1_accountClaims = JWT.Claims.of(_1_account.getId(), _1_account.getUsername(), _1_account.getNickname(), null,new String[]{USER.value()});
        _1_accountToken = jwt.newToken(_1_accountClaims);
        JWT.Claims _2_accountClaims = JWT.Claims.of(_2_account.getId(), _2_account.getUsername(), _2_account.getNickname(), null,new String[]{USER.value()});
        _2_accountToken = jwt.newToken(_2_accountClaims);
        JWT.Claims _3_accountClaims = JWT.Claims.of(_3_account.getId(), _3_account.getUsername(), _3_account.getNickname(),null, new String[]{USER.value()});
        _3_accountToken = jwt.newToken(_3_accountClaims);
        JWT.Claims _27_accountClaims = JWT.Claims.of(_27_account.getId(), _27_account.getUsername(), _27_account.getNickname(), null,new String[]{USER.value()});
        _27_accountToken = jwt.newToken(_27_accountClaims);
    }

    /**
     * 이미 팔로우 하고 있는 상태에서 또 팔로우 요청을 했을 경우, Bad Request 로 응답한다.
     * 이미 2번 계정은 1번 계정을 팔로우 하고 있는 상태.
     */
    @Test
    @Transactional
    public void followTestWhenAlreadyFollow() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/account/follow/1")
                .header("Authorization", "Bearer " + _2_accountToken)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    /**
     * 3번 계정은 존재 하지 않는 계정이다.
     * 접근자(principal) 계정이 존재 하지 않는 계정일 경우, 401 (Unauthorized) 로 응답한다.
     */
    @Test
    @Transactional
    public void followTestWhenPrincipalAccountIsNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/account/follow/1")
                .header("Authorization", "Bearer " + _3_accountToken)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    /**
     * 3번 계정은 존재 하지 않는 계정이다.
     * 팔로우 하려는 대상 계정이 존재 하지 않는 계정일 경우, Not Found 로 응답한다.
     */
    @Test
    @Transactional
    public void followTestWhenTargetAccountIsNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/account/follow/3")
                .header("Authorization", "Bearer " + _2_accountToken)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    /**
     * 팔로우 하고 있지 않은 상태에서 팔로우 요청 테스트.
     * 27번 계정이 1번 계정을 팔로우 요청 테스트.
     */
    @Test
    @Transactional
    public void followTestWhenHaveNeverBeen() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/account/follow/1")
                .header("Authorization", "Bearer " + _27_accountToken)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    /**
     * 2번 계정이 1번 계정을 팔로우 하고 있는 상태에서, 언팔 테스트
     */
    @Test
    @Transactional
    public void unFollowTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/account/follow/1")
                .header("Authorization", "Bearer " + _2_accountToken)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * 3번 계정은 존재 하지 않는 계정이다.
     * 언팔로우 하는 대상 계정이 존재 하지 않는 계정일 경우, 404 (Not Found) 로 응답한다.
     */
    @Test
    @Transactional
    public void unFollowTestWhenPrincipalAccountIsNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/account/follow/3")
                .header("Authorization", "Bearer " + _2_accountToken)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    /**
     * 3번 계정은 존재 하지 않는 계정이다.
     * 접근자(principal) 계정이 존재 하지 않는 계정일 경우, 401 (Unauthorized) 로 응답한다.
     */
    @Test
    @Transactional
    public void unFollowTestWhenTargetAccountIsNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/account/follow/1")
                .header("Authorization", "Bearer " + _3_accountToken)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    /**
     * 2번 계정을 팔로우하고 있는 모든 계정 정보 테스트
     */
    @Test
    @Transactional
    public void getFollowerTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/account/followers/2")
                .header("Authorization", "Bearer " + _1_accountToken)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * 3번 계정은 존재 하지 않는 계정이다.
     * 접근자(principal) 계정이 존재 하지 않는 계정일 경우, 401 (Unauthorized) 로 응답한다.
     */
    @Test
    @Transactional
    public void getFollowerTestWhenPrincipalAccountIsNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/account/followers/2")
                .header("Authorization", "Bearer " + _3_accountToken)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    /**
     * 3번 계정은 존재 하지 않는 계정이다.
     * 모든 팔로워를 가져 오려고 하는 대상 계정이 존재 하지 않는 계정일 경우, 404 (Not Found) 로 응답한다.
     */
    @Test
    @Transactional
    public void getFollowerTestWhenTargetAccountIsNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/account/followers/3")
                .header("Authorization", "Bearer " + _1_accountToken)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    /**
     * 1번 계정이 팔로잉하고 있는 모든 계정 정보 테스트
     */
    @Test
    @Transactional
    public void getFollowingTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/account/followings/1")
                .header("Authorization", "Bearer " + _1_accountToken)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * 3번 계정은 존재 하지 않는 계정이다.
     * 접근자(principal) 계정이 존재 하지 않는 계정일 경우, 401 (Unauthorized) 로 응답한다.
     */
    @Test
    @Transactional
    public void getFollowingTestWhenPrincipalAccountIsNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/account/followings/1")
                .header("Authorization", "Bearer " + _3_accountToken)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    /**
     * 3번 계정은 존재 하지 않는 계정이다.
     * 팔로잉 하고 있는 계정을 가져 오려고 하는 대상 계정이 존재 하지 않는 계정일 경우, 404 (Not Found) 로 응답한다.
     */
    @Test
    @Transactional
    public void getFollowingTestWhenTargetAccountIsNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/account/followings/3")
                .header("Authorization", "Bearer " + _1_accountToken)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}




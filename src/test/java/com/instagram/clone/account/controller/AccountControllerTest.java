package com.instagram.clone.account.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.instagram.clone.account.model.dto.request.SignUpRequestDTO;
import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.account.repository.AccountRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.Random;
import java.util.stream.IntStream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Eunsoo Kim
 * @since 2019-09-22
 */


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AccountRepository accountRepository;


    @Test
    @Transactional
    public void signUpTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        SignUpRequestDTO userInfo = new SignUpRequestDTO();

        userInfo.setUsername(createRandomUserName());
        userInfo.setPassword("test");
        userInfo.setNickname("test");

        mockMvc.perform(post("/api/account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userInfo)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private String createRandomUserName(){
        String userName;
        final char[] chars = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        do {
            userName = getRandomString(chars);
        } while (hasUserName(userName));
        return userName;
    }

    private String getRandomString(char[] chars){
        StringBuilder buffer = new StringBuilder();
        Random random = new Random();
        final int maxLength = 8;
        IntStream.rangeClosed(0, maxLength)
                 .forEach(i-> buffer.append(chars[random.nextInt(chars.length)]));
        return buffer.toString();
    }
    private boolean hasUserName(String userName){
        return  null != accountRepository.findByUsername(userName);
    }
}
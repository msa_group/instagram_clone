package com.instagram.clone.comment;

import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.account.repository.AccountRepository;
import com.instagram.clone.comment.model.entity.Comment;
import com.instagram.clone.comment.repository.CommentRepository;
import com.instagram.clone.post.model.entity.Post;
import com.instagram.clone.post.repository.PostRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@SpringBootTest
@Transactional
public class CommentRepositoryTest {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PostRepository postRepository;

    private Account account;
    private Post post;
    private Comment comment;

    @BeforeEach
    public void setUp() {
        account = Account.builder()
                .username("testUserName")
                .password("testPassword")
                .nickname("testNickName")
                .build();

        post = Post.builder()
                .author(account)
                .content("test Post Content")
                .postDt(Instant.now())
                .updateDt(Instant.now())
                .build();

        accountRepository.save(account);
        postRepository.save(post);
        comment = commentRepository.save(Comment.builder()
                .author(account)
                .parentPost(post)
                .content("test comment content")
                .postDt(Instant.now())
                .build());
    }

    @Test
    public void findByIdTest() {
        log.info("comment => {}", comment);
        Optional<Comment> findCommentOptional = commentRepository.findById(comment.getId());
        Assertions.assertTrue(findCommentOptional.isPresent());
        log.info("comment => {}", findCommentOptional.orElse(Comment.builder().build()).toString());
    }

    @Test
    @Rollback
    public void saveTest() {
        final String  commentContent = "save comment test";
        final Instant commentPostDt = Instant.now();
        Comment saveComment = commentRepository.save(Comment.builder()
                .author(account)
                .parentPost(post)
                .content(commentContent)
                .postDt(commentPostDt)
                .build());
        Assertions.assertTrue(Objects.nonNull(saveComment.getId()));
        Assertions.assertTrue(StringUtils.equals(commentContent, saveComment.getContent()));
        Assertions.assertTrue(saveComment.getPostDt().equals(commentPostDt));
        log.info("saveComment => {}", saveComment.toString());
    }

    @Test
    @Rollback
    public void deleteTest() {
        Optional<Comment> findCommentOptional = commentRepository.findById(comment.getId());
        Assertions.assertTrue(findCommentOptional.isPresent());
        commentRepository.delete(findCommentOptional.get());
        Assertions.assertEquals(commentRepository.findById(comment.getId()).isPresent(), false);
    }
}

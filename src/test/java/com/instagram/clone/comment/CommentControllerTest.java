package com.instagram.clone.comment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.instagram.clone.InstagramApplicationTests;
import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.account.repository.AccountRepository;
import com.instagram.clone.account.repository.JWTRepository;
import com.instagram.clone.comment.model.dto.request.CreateCommentRequestDTO;
import com.instagram.clone.comment.model.dto.request.ModifyCommentRequestDTO;
import com.instagram.clone.comment.model.dto.response.CommentResponseDTO;
import com.instagram.clone.comment.model.entity.Comment;
import com.instagram.clone.comment.service.CommentService;
import com.instagram.clone.common.security.JWT;
import com.instagram.clone.post.model.entity.Post;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.Instant;

import static com.instagram.clone.common.security.model.Role.USER;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureMockMvc
public class CommentControllerTest extends InstagramApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JWT jwt;

    @Autowired
    private JWTRepository jwtRepository;

    @Autowired
    private AccountRepository accountRepository;

    @MockBean
    private CommentService commentService;

    private Comment comment;
    private Post post;
    private ObjectMapper objectMapper;

    private String token;


    @BeforeAll
    public void setUp() {
        objectMapper = new ObjectMapper();
        Account account = Account.builder()
                .id(1L)
                .username("testUserName")
                .nickname("testNickName")
                .password("testPassword")
                .isActive(true)
                .build();
        post = Post.builder()
                .id(1)
                .author(account)
                .content("test Post Content")
                .postDt(Instant.now())
                .updateDt(Instant.now())
                .build();

        comment= Comment.builder()
                .author(account)
                .id(1L)
                .parentPost(post)
                .content("test comment content")
                .postDt(Instant.now())
                .build();

        JWT.Claims claims = JWT.Claims.of(account.getId(), account.getUsername(),
                account.getNickname(), null,new String[]{USER.value()});
        token = jwt.newToken(claims);
        jwtRepository.save(claims.getRepositoryKey(), token);
        account.addComment(comment);
        post.addComment(comment);
        accountRepository.save(account);
    }

    @Test
    public void createCommentTest() throws Exception {
        final String createContent = "creata comment content";
        CreateCommentRequestDTO createCommentRequestDTO =
                new CreateCommentRequestDTO(1, createContent);
        CommentResponseDTO commentResponseDTO =
                CommentResponseDTO.builder()
                        .id(2L)
                        .content(createContent)
                        .parentPostId(post.getId())
                        .authorId(1L)
                        .build();
        final String json = objectMapper.writeValueAsString(createCommentRequestDTO);
        when(commentService.createComment("testUserName", createCommentRequestDTO)).thenReturn(commentResponseDTO);
        when(commentService.getComment(2)).thenReturn(commentResponseDTO);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/comments")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .header("Authorization", "Bearer " + token))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getCommentTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/api/comments/" + comment.getId())
                .header("Authorization", "Bearer " + token))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void modifyCommentTest() throws Exception {
        ModifyCommentRequestDTO modifyCommentRequestDTO =
                new ModifyCommentRequestDTO("modify comment content");
        final String json = objectMapper.writeValueAsString(modifyCommentRequestDTO);
        mockMvc.perform(MockMvcRequestBuilders
                .put("/api/comments/" + comment.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(json)
                .header("Authorization", "Bearer " + token))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void destroyCommentTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/comments/" + comment.getId())
                .header("Authorization", "Bearer " + token))
                .andDo(print())
                .andExpect(status().isNoContent());
    }
}

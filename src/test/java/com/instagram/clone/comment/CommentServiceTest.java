package com.instagram.clone.comment;

import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.account.repository.AccountRepository;
import com.instagram.clone.comment.model.dto.request.CreateCommentRequestDTO;
import com.instagram.clone.comment.model.dto.request.ModifyCommentRequestDTO;
import com.instagram.clone.comment.model.dto.response.CommentResponseDTO;
import com.instagram.clone.comment.model.entity.Comment;
import com.instagram.clone.comment.repository.CommentRepository;
import com.instagram.clone.comment.service.CommentService;
import com.instagram.clone.common.error.NotFoundException;
import com.instagram.clone.post.model.entity.Post;
import com.instagram.clone.post.repository.PostRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Objects;

@Slf4j
@SpringBootTest
@Transactional
public class CommentServiceTest {

    @Autowired
    private CommentService commentService;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentRepository commentRepository;

    private Account account;
    private Post post;
    private Comment comment;

    @BeforeEach
    public void setUp() {
        final String userName = "testUserName";
        Account setUpAccount = Account.builder()
                .username(userName)
                .password("testPassword")
                .nickname("testNickName")
                .isActive(true)
                .build();

        post = Post.builder()
                .author(setUpAccount)
                .content("test Post Content")
                .postDt(Instant.now())
                .updateDt(Instant.now())
                .build();

        comment= Comment.builder()
                .author(setUpAccount)
                .parentPost(post)
                .content("test comment content")
                .postDt(Instant.now())
                .build();
        post.addComment(comment);
        setUpAccount.addComment(comment);
        account = accountRepository.save(setUpAccount);
    }

    @Test
    public void createCommentExceptionTest() {
        final long dummyPostId = 1234;
        Assertions.assertThrows(NotFoundException.class,
                () -> commentService.createComment(account.getUsername(),
                        new CreateCommentRequestDTO(dummyPostId, "dummy Comment Conetent")));
    }

    @Test
    @Rollback
    public void createCommentTest() {
        final String commentContent = "test comment content";
        final CreateCommentRequestDTO createCommentRequestDTO =
                new CreateCommentRequestDTO(post.getId(), commentContent);

        CommentResponseDTO commentResponseDTO =
                commentService.createComment(account.getUsername(), createCommentRequestDTO);

        Assertions.assertEquals(true, Objects.nonNull(commentResponseDTO));
        Assertions.assertEquals(true, Objects.nonNull(commentResponseDTO.getId()));
        Assertions.assertEquals(account.getId(), commentResponseDTO.getAuthorId());
        Assertions.assertEquals(post.getId(), commentResponseDTO.getParentPostId());
        Assertions.assertTrue(StringUtils.equals(commentContent, commentResponseDTO.getContent()));
    }

    @Test
    public void getCommentExceptionTest() {
        Assertions.assertThrows(NotFoundException.class, () -> {
           commentService.getComment(1234);
        });
    }

    @Test
    @Rollback
    public void modifyCommentTest() {
        final String commentModifyContent = "test modify comment";
        ModifyCommentRequestDTO modifyCommentRequestDTO =
                new ModifyCommentRequestDTO(commentModifyContent);
        CommentResponseDTO commentResponseDTO = commentService.modifyComment(
                comment.getId(),
                modifyCommentRequestDTO
        );
        Assertions.assertEquals(true, Objects.nonNull(commentResponseDTO));
        Assertions.assertTrue(StringUtils.equals(commentModifyContent, commentResponseDTO.getContent()));
    }

    @Test
    public void destroyCommentTest() {
        final long dummyCommentId = 1234L;
        Assertions.assertThrows(NotFoundException.class, () -> {
           commentService.destroyComment(dummyCommentId);
        });
    }
}

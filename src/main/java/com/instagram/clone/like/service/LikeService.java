package com.instagram.clone.like.service;

import com.instagram.clone.like.dto.request.LikeRequest;
import com.instagram.clone.like.dto.response.LikeDTO;

public interface LikeService {

    LikeDTO like(LikeRequest request);
}

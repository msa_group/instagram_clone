package com.instagram.clone.like.service;

import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.account.repository.AccountRepository;
import com.instagram.clone.comment.model.entity.Comment;
import com.instagram.clone.comment.repository.CommentRepository;
import com.instagram.clone.common.error.NotFoundException;
import com.instagram.clone.like.dto.request.CommentLikeRequest;
import com.instagram.clone.like.dto.request.LikeRequest;
import com.instagram.clone.like.dto.request.PostLikeRequest;
import com.instagram.clone.like.dto.response.CommentLikeDTO;
import com.instagram.clone.like.dto.response.LikeDTO;
import com.instagram.clone.like.dto.response.PostLikeDTO;
import com.instagram.clone.like.model.entity.Like;
import com.instagram.clone.like.repository.LikeRepository;
import com.instagram.clone.post.model.entity.Post;
import com.instagram.clone.post.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.instagram.clone.like.model.entity.Like.LikeType.COMMENT;
import static com.instagram.clone.like.model.entity.Like.LikeType.POST;

@Slf4j
@Service
@RequiredArgsConstructor
public class LikeServiceImpl implements LikeService {

    private final AccountRepository accountRepository;

    private final PostRepository postRepository;

    private final LikeRepository likeRepository;

    private final CommentRepository commentRepository;

    @Override
    public LikeDTO like(LikeRequest request) {
        if (request instanceof PostLikeRequest) {
            return likePost((PostLikeRequest)request);
        }

        return likeComment((CommentLikeRequest)request);
    }

    /**
     * post 좋아요 기능
     */
    private LikeDTO likePost(PostLikeRequest request) {
        if (checkAlreadyPostLike(request)) {
            return PostLikeDTO.NONE;
        }

        long postId = request.getId().value();

        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new NotFoundException(Post.class, postId, "해당 post가 존재하지 않습니다."));

        Account account = findById(request.getJwtAuthentication().getId().value());

        Like like = Like.builder()
                .account(account)
                .post(post)
                .likeType(POST)
                .build();

        return PostLikeDTO.of(likeRepository.save(like));
    }

    /**
     * comment 좋아요 기능
     */
    private LikeDTO likeComment(CommentLikeRequest request) {
        if (checkAlreadyCommentLike(request)) {
            return CommentLikeDTO.NONE;
        }

        long commentId = request.getId().value();

        Comment comment = commentRepository.findById(commentId)
                .orElseThrow(() -> new NotFoundException(Comment.class, commentId, "해당 comment가 존재하지 않습니다."));

        Account account = findById(request.getJwtAuthentication().getId().value());

        Like like = Like.builder()
                .account(account)
                .comment(comment)
                .likeType(COMMENT)
                .build();

        return CommentLikeDTO.of(likeRepository.save(like));
    }

    /**
     * PK로 Account get
     */
    private Account findById(long id){
        return accountRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(Post.class, id, "해당 account가 존재하지 않습니다."));
    }

    /**
     * 이미 좋아요를 한 post 라면 좋아요 취소 기능.
     */
    private boolean checkAlreadyPostLike(PostLikeRequest request) {
        Optional<Like> like = Optional.ofNullable(likeRepository.findByAccountIdAndPostId(
                        request.getJwtAuthentication().getId().value(),
                        request.getId().value()));
        boolean isExist = like.isPresent();

        if (isExist) {
            likeRepository.delete(like.get());
        }

        return isExist;
    }

    /**
     * 이미 좋아요를 한 comment 라면 좋아요 취소 기능.
     */
    private boolean checkAlreadyCommentLike(CommentLikeRequest request) {
        Optional<Like> like =  Optional.ofNullable(likeRepository.findByAccountIdAndCommentId(
                request.getJwtAuthentication().getId().value(),
                request.getId().value()));

        boolean isExist = like.isPresent();
        if (isExist) {
            likeRepository.delete(like.get());
        }

        return isExist;

    }

}

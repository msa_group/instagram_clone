package com.instagram.clone.like.controller;

import com.instagram.clone.comment.model.entity.Comment;
import com.instagram.clone.common.api.ApiResult;
import com.instagram.clone.common.common.Id;
import com.instagram.clone.common.security.JwtAuthentication;
import com.instagram.clone.like.dto.request.CommentLikeRequest;
import com.instagram.clone.like.dto.request.PostLikeRequest;
import com.instagram.clone.like.dto.response.LikeDTO;
import com.instagram.clone.like.service.LikeService;
import com.instagram.clone.post.model.entity.Post;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.instagram.clone.common.api.ApiResult.OK;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/likes")
public class LikeController {

    private final LikeService likeService;


    @PostMapping("/post/{postId}")
    public ResponseEntity<ApiResult<LikeDTO>> likePost(@AuthenticationPrincipal JwtAuthentication jwtAuthentication, @PathVariable long postId){
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(OK(likeService.like(PostLikeRequest.create(jwtAuthentication, Id.of(Post.class, postId)))));
    }

    @PostMapping("/comment/{commentId}")
    public ResponseEntity<ApiResult<LikeDTO>> likeComment(@AuthenticationPrincipal JwtAuthentication jwtAuthentication, @PathVariable long commentId) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(OK(likeService.like(CommentLikeRequest.create(jwtAuthentication, Id.of(Comment.class, commentId)))));
    }
}

package com.instagram.clone.like.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.instagram.clone.like.model.entity.Like;
import lombok.Getter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PostLikeDTO implements LikeDTO {
    public static final PostLikeDTO NONE = new PostLikeDTO();

    @Getter
    @JsonUnwrapped
    private Like like;

    private PostLikeDTO(){}

    public static LikeDTO of(Like like) {
        PostLikeDTO dto = new PostLikeDTO();
        dto.like = like;
        return dto;
    }

    @Override
    public String toString() {
        return new ReflectionToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .toString();
    }
}

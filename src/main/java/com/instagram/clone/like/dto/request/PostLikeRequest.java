package com.instagram.clone.like.dto.request;

import com.instagram.clone.common.common.Id;
import com.instagram.clone.common.security.JwtAuthentication;
import com.instagram.clone.post.model.entity.Post;
import lombok.Getter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Getter
public final class PostLikeRequest implements LikeRequest{
    private Id<Post, Long> id;
    private JwtAuthentication jwtAuthentication;

    private PostLikeRequest(){}

    public static LikeRequest create(JwtAuthentication jwtAuthentication, Id<Post, Long> postId) {
        PostLikeRequest request = new PostLikeRequest();
        request.id = postId;
        request.jwtAuthentication = jwtAuthentication;
        return request;
    }

    @Override
    public String toString() {
        return new ReflectionToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .toString();
    }
}

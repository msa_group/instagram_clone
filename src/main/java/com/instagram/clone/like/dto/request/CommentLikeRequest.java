package com.instagram.clone.like.dto.request;

import com.instagram.clone.comment.model.entity.Comment;
import com.instagram.clone.common.common.Id;
import com.instagram.clone.common.security.JwtAuthentication;
import lombok.Getter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Getter
public final class CommentLikeRequest implements LikeRequest{
    private Id<Comment, Long> id;
    private JwtAuthentication jwtAuthentication;

    private CommentLikeRequest(){}

    public static LikeRequest create(JwtAuthentication jwtAuthentication, Id<Comment, Long> postId) {
        CommentLikeRequest request = new CommentLikeRequest();
        request.id = postId;
        request.jwtAuthentication = jwtAuthentication;
        return request;
    }

    @Override
    public String toString() {
        return new ReflectionToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .toString();
    }
}

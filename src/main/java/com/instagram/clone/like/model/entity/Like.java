package com.instagram.clone.like.model.entity;

import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.comment.model.entity.Comment;
import com.instagram.clone.post.model.entity.Post;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="insta_like")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@SequenceGenerator(name="like_id_sequence",
        sequenceName="like_id_sequence",
        allocationSize = 25)
public class Like {

    public static enum LikeType {POST, COMMENT}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "like_id_sequence")
    private long id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "account_id", nullable = false)
    private Account account;

    @Enumerated(EnumType.STRING)
    private LikeType likeType;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "post_id")
    private Post post;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "comment_id")
    private Comment comment;
}

package com.instagram.clone.like.repository;

import com.instagram.clone.like.model.entity.Like;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LikeRepository extends JpaRepository<Like, Long> {

    Like findByAccountIdAndPostId(long accountId, long postId);

    Like findByAccountIdAndCommentId(long accountId, long commentId);
}

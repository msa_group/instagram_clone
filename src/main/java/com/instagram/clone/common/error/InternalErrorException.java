package com.instagram.clone.common.error;

import com.instagram.clone.common.util.MessageUtils;

public class InternalErrorException extends ServiceRuntimeException {

    private static final String MESSAGE_KEY = "error.internal";
    private static final String MESSAGE_DETAILS = "error.internal.details";

    public InternalErrorException(String message) {
        super(MESSAGE_KEY, MESSAGE_DETAILS, new Object[]{message});
    }

    @Override
    public String getMessage() {
        return MessageUtils.getInstance().getMessage(getDetailKey(), getParams());
    }

    @Override
    public String toString() {
        return MessageUtils.getInstance().getMessage(getMessageKey());
    }
}

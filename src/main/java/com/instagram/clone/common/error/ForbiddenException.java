package com.instagram.clone.common.error;

import com.instagram.clone.common.util.MessageUtils;

public class ForbiddenException extends ServiceRuntimeException {

    private static final String MESSAGE_KEY = "error.forbidden";
    private static final String MESSAGE_DETAILS = "error.forbidden.details";

    public ForbiddenException(String message) {
        super(MESSAGE_KEY, MESSAGE_DETAILS, new Object[]{message});
    }

    @Override
    public String getMessage() {
        return MessageUtils.getInstance().getMessage(getDetailKey(), getParams());
    }

    @Override
    public String toString() {
        return MessageUtils.getInstance().getMessage(getMessageKey());
    }
}

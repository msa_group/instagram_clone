package com.instagram.clone.common.error;

import com.instagram.clone.common.util.MessageUtils;

public class BadRequestException extends ServiceRuntimeException {

    private static final String MESSAGE_KEY = "error.badrequest";
    private static final String MESSAGE_DETAILS = "error.badrequest.details";

    public BadRequestException(String message) {
        super(MESSAGE_KEY, MESSAGE_DETAILS, new Object[]{message});
    }

    @Override
    public String getMessage() {
        return MessageUtils.getInstance().getMessage(getDetailKey(), getParams());
    }

    @Override
    public String toString() {
        return MessageUtils.getInstance().getMessage(getMessageKey());
    }
}

package com.instagram.clone.common.common;

/**
 * @author Eunsoo Kim
 * @since 2019-10-13
 */
public class AccountContext {
    private static final ThreadLocal<String> ACCOUNT_THREAD_LOCAL = new ThreadLocal<>();
    public static void setAccountName(String name){
        ACCOUNT_THREAD_LOCAL.set(name);
    }
    public static String getAccountName(){
        return ACCOUNT_THREAD_LOCAL.get();
    }
}

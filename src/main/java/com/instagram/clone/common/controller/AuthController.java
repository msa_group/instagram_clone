package com.instagram.clone.common.controller;

import com.instagram.clone.common.api.ApiResult;
import com.instagram.clone.common.error.UnauthorizedException;
import com.instagram.clone.common.security.AuthenticationRequest;
import com.instagram.clone.common.security.AuthenticationResult;
import com.instagram.clone.common.security.JwtAuthentication;
import com.instagram.clone.common.security.JwtAuthenticationToken;
import com.instagram.clone.common.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import static com.instagram.clone.common.api.ApiResult.OK;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class AuthController {

    private final AuthenticationManager authenticationManager;
    private final AuthService authService;

    @PostMapping("/auth")
    public ResponseEntity<ApiResult<AuthenticationResult> >authentication(@RequestBody AuthenticationRequest authRequest, HttpServletRequest request) throws UnauthorizedException {
        try {
            String userAgent = request.getHeader("User-Agent").toUpperCase();
            JwtAuthenticationToken authToken = new JwtAuthenticationToken(authRequest.getPrincipal(), authRequest.getCredentials(),userAgent);
            Authentication authentication = authenticationManager.authenticate(authToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return ResponseEntity.ok().body(OK((AuthenticationResult) authentication.getDetails()));
        } catch (AuthenticationException e) {
            throw new UnauthorizedException(e.getMessage());
        }
    }

    @DeleteMapping("/auth")
    public ResponseEntity<ApiResult<String>> signOut(HttpServletRequest request, @AuthenticationPrincipal JwtAuthentication jwtAuthentication){
        return ResponseEntity.ok().body(authService.signOut(request, jwtAuthentication));
    }
}

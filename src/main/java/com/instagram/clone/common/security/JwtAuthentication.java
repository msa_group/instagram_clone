package com.instagram.clone.common.security;

import com.instagram.clone.account.model.dto.response.LoginResponseDTO;
import com.instagram.clone.common.common.Id;
import lombok.Getter;

import static com.google.common.base.Preconditions.checkNotNull;

@Getter
public class JwtAuthentication {

    public final Id<LoginResponseDTO, Long> id;

    private final String userName;

    private final String nickName;

    public JwtAuthentication(Long id, String userName, String nickName) {
        checkNotNull(id, "id must be provided.");
        checkNotNull(userName, "name must be provided.");
        checkNotNull(nickName, "email must be provided.");

        this.id = Id.of(LoginResponseDTO.class, id);
        this.userName = userName;
        this.nickName = nickName;
    }

}
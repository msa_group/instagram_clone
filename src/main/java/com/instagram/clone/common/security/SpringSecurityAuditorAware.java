package com.instagram.clone.common.security;

import com.instagram.clone.common.common.AccountContext;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;


/**
 * @author Eunsoo Kim
 * @since 2019-10-09
 */

@Service
public class SpringSecurityAuditorAware implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Optional<String> retVal;
        String username;
        if(null == authentication || !authentication.isAuthenticated()){
            retVal = Optional.empty();
        }
        else if(authentication instanceof AnonymousAuthenticationToken){
            username = AccountContext.getAccountName();
            checkNotNull(username, "username must be provided");
            retVal = Optional.ofNullable(username);
        }
        else{
            username = ((JwtAuthentication) (authentication.getPrincipal())).getUserName();
            retVal = Optional.ofNullable(username);
        }
        return retVal;
    }
}

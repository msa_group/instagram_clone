package com.instagram.clone.common.security;

import com.instagram.clone.account.model.dto.response.LoginResponseDTO;
import com.instagram.clone.account.repository.JWTRepository;
import com.instagram.clone.account.service.AccountService;
import com.instagram.clone.common.error.NotFoundException;
import com.instagram.clone.common.security.model.Role;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.apache.commons.lang3.ClassUtils.isAssignable;
import static org.springframework.security.core.authority.AuthorityUtils.createAuthorityList;

@Slf4j
public class JwtAuthenticationProvider implements AuthenticationProvider {

    private final JWT jwt;

    private final AccountService accountService;

    private final JWTRepository jwtRepository;

    public JwtAuthenticationProvider(JWT jwt, AccountService accountService, JWTRepository jwtRepository) {
        this.jwt = jwt;
        this.accountService = accountService;
        this.jwtRepository = jwtRepository;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        JwtAuthenticationToken authenticationToken = (JwtAuthenticationToken) authentication;
        return processUserAuthentication(authenticationToken.authenticationRequest());
    }

    private Authentication processUserAuthentication(AuthenticationRequest request) {
        try {
            LoginResponseDTO loginResponseDTO = accountService.signIn(request.getPrincipal(), request.getCredentials());
            JwtAuthenticationToken authenticated =
                    new JwtAuthenticationToken(loginResponseDTO.getId(), null, request.getUserAgent(), createAuthorityList(Role.USER.value()));

            String apiToken = loginResponseDTO.newApiToken(jwt, request.getUserAgent(), new String[]{Role.USER.value()});
            JWT.Claims claim = JWT.Claims.of(0L, loginResponseDTO.getUsername(), null, request.getUserAgent(), null);
            jwtRepository.save(claim.getRepositoryKey(), apiToken);

            authenticated.setDetails(new AuthenticationResult(apiToken, loginResponseDTO));
            return authenticated;
        } catch (NotFoundException e) {
            throw new UsernameNotFoundException(e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new BadCredentialsException(e.getMessage());
        } catch (DataAccessException e) {
            throw new AuthenticationServiceException(e.getMessage(), e);
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return isAssignable(JwtAuthenticationToken.class, authentication);
    }

}
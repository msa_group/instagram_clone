package com.instagram.clone.common.security;

import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.account.repository.AccountRepository;
import com.instagram.clone.account.repository.JWTRepository;
import com.instagram.clone.account.service.AccountService;
import com.instagram.clone.common.error.NotFoundException;
import com.instagram.clone.common.util.JWTUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@Slf4j
public class JwtAuthenticationTokenFilter extends GenericFilterBean {

    @Autowired
    private JWT jwt;

    @Autowired
    private JWTRepository jwtRepository;

    @Autowired
    private JWTUtils jwtUtils;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountService accountService;

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            String authorizationToken = jwtUtils.obtainAuthorizationToken(request);
            if (authorizationToken != null) {
                try {
                    JWT.Claims claims = verify(authorizationToken);
                    String redisKey = claims.getRepositoryKey();
                    log.debug("Jwt parse result: {}", claims);
                    if(!verifyExpiredToken(redisKey, authorizationToken)){
                        throw new Exception("Token has expired. Username(" + claims.userName + ")");
                    }

                    // 만료 10분 전
                    if (canRefresh(claims, 6000 * 10)) {
                        String refreshedToken = jwt.refreshToken(authorizationToken);
                        response.setHeader(jwtUtils.getTokenHeader(), refreshedToken);
                        jwtRepository.save(redisKey, refreshedToken);
                    }

                    Long userKey = claims.userKey;
                    if (accountService.isNotExists(userKey)) {
                        throw new NotFoundException(Account.class, userKey);
                    }
                    String userName = claims.userName;
                    String nickName = claims.nickName;
                    List<GrantedAuthority> authorities = obtainAuthorities(claims);

                    if(!verifyAccountActivation(claims)){
                        throw new Exception("Try to access inactive account. Username(" + claims.userName + ")");
                    }

                    if (nonNull(userKey) && isNotEmpty(userName) && nonNull(nickName) && !authorities.isEmpty()) {
                        JwtAuthenticationToken authentication =
                                new JwtAuthenticationToken(new JwtAuthentication(userKey, userName, nickName), null,null, authorities);
                        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                        SecurityContextHolder.getContext().setAuthentication(authentication);
                    }
                } catch (Exception e) {
                    log.warn("Jwt processing failed: {}", e.getMessage());
                }
            }
        }
        else {
            log.debug("SecurityContextHolder not populated with security token, as it already contained: '{}'",
                    SecurityContextHolder.getContext().getAuthentication());
        }

        chain.doFilter(request, response);
    }

    private boolean canRefresh(JWT.Claims claims, long refreshRangeMillis) {
        long exp = claims.exp();
        if (exp > 0) {
            long remain = exp - System.currentTimeMillis();
            return remain < refreshRangeMillis;
        }
        return false;
    }

    private boolean verifyExpiredToken(String userKey, String jwtToken) {
        return jwtToken.equals(jwtRepository.findByUserKey(userKey));
    }

    private boolean verifyAccountActivation(JWT.Claims claims){
        return accountRepository.findByUsername(claims.userName).isActive();
    }

    private List<GrantedAuthority> obtainAuthorities(JWT.Claims claims) {
        String[] roles = claims.roles;
        return roles == null || roles.length == 0 ?
                Collections.emptyList() :
                Arrays.stream(roles).map(SimpleGrantedAuthority::new).collect(toList());
    }


    private JWT.Claims verify( String token) {
        return jwt.verify(token);
    }
}
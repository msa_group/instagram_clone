package com.instagram.clone.common.security;

import com.instagram.clone.account.model.dto.response.LoginResponseDTO;
import lombok.Getter;

import static com.google.common.base.Preconditions.checkNotNull;

@Getter
public class AuthenticationResult {

    private final String apiToken;

    private final LoginResponseDTO user;

    AuthenticationResult(String apiToken, LoginResponseDTO user) {
        checkNotNull(apiToken, "apiToken must be provided.");
        checkNotNull(user, "user must be provided.");

        this.apiToken = apiToken;
        this.user = user;
    }

}

package com.instagram.clone.common.converter;

import javax.persistence.AttributeConverter;

/**
 * @author Eunsoo Kim
 * @since 2019-10-19
 */
public class BooleanToYNConverter implements AttributeConverter<Boolean, String> {
    @Override
    public String convertToDatabaseColumn(Boolean attribute) {
        return (null != attribute && attribute) ? "Y" : "N";
    }

    @Override
    public Boolean convertToEntityAttribute(String dbData) {
        return "Y".equals(dbData);
    }
}

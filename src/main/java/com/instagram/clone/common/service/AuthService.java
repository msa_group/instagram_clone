package com.instagram.clone.common.service;

import com.instagram.clone.common.api.ApiResult;
import com.instagram.clone.common.security.JwtAuthentication;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Eunsoo Kim
 * @since 2019-09-29
 */
public interface AuthService {
    ApiResult<String> signOut(HttpServletRequest request, JwtAuthentication jwtAuthentication);
}

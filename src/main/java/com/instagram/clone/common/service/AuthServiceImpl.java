package com.instagram.clone.common.service;

import com.instagram.clone.account.repository.JWTRepository;
import com.instagram.clone.common.api.ApiResult;
import com.instagram.clone.common.security.JWT;
import com.instagram.clone.common.security.JwtAuthentication;
import com.instagram.clone.common.util.JWTUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

import static com.instagram.clone.common.api.ApiResult.OK;

/**
 * @author Eunsoo Kim
 * @since 2019-09-29
 */

@Slf4j
@RequiredArgsConstructor
@Service
public class AuthServiceImpl implements AuthService {
    private final JWTRepository jwtRepository;
    private final JWT jwt;
    private final JWTUtils jwtUtils;

    @Override
    public ApiResult<String> signOut(HttpServletRequest request, JwtAuthentication jwtAuthentication) {
        JWT.Claims claims = jwt.verify(jwtUtils.obtainAuthorizationToken(request));
        jwtRepository.delete(claims.getRepositoryKey());
        return OK("You have been signed out successfully");
    }
}

package com.instagram.clone.common.util;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;
import java.util.regex.Pattern;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * @author Eunsoo Kim
 * @since 2019-09-29
 */

@Slf4j
@Component
public class JWTUtils {
    @Value("${jwt.token.header}") @Getter
    private String tokenHeader;
    private final Pattern BEARER = Pattern.compile("^Bearer$", Pattern.CASE_INSENSITIVE);

    public  String obtainAuthorizationToken(HttpServletRequest request) {

        String token = request.getHeader(tokenHeader);
        if (token != null) {
            if (log.isDebugEnabled())
                log.debug("Jwt authorization api detected: {}", token);
            token = URLDecoder.decode(token, UTF_8);
            String[] parts = token.split(" ");
            if (parts.length == 2) {
                String scheme = parts[0];
                String credentials = parts[1];
                return BEARER.matcher(scheme).matches() ? credentials : null;
            }
        }
        return null;
    }


}

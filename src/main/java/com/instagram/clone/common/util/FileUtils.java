package com.instagram.clone.common.util;

import com.google.common.base.MoreObjects;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

public class FileUtils {


    /**
     * 외부 소스
     * Multipart File을 S3에 업로드 가능하게 File 로 변경한다
     * https://reviewdb.io/posts/1504810616200/how-to-convert-multipartfile-to-java-io-file-in-spring
     * @param file
     * @return
     * @throws IOException
     */
    public static File convertToFile(MultipartFile file, String fileName) throws IOException {
        File convFile = new File(fileName);
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }
}

package com.instagram.clone.common.type;

public enum ValidImgType {
    JPG, JPEG, BMP, PNG, GIF
}

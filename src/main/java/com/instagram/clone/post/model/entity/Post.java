package com.instagram.clone.post.model.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.attachment.model.entity.Attachment;
import com.instagram.clone.comment.model.entity.Comment;
import com.instagram.clone.like.model.entity.Like;
import com.instagram.clone.post.model.dto.response.EditPostResponseDTO;
import com.instagram.clone.post.model.dto.response.GetPostResponseDTO;
import com.instagram.clone.post.model.dto.response.NewPostResponseDTO;
import lombok.*;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="post")
@SequenceGenerator(name="post_id_sequence",
        sequenceName="post_id_sequence",
        allocationSize = 25)
@Builder(toBuilder = true)
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "post_id_sequence")
    private long id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "account_id", nullable = false)
    private Account author;

    @Column(columnDefinition = "TEXT", nullable = false)
    @Setter
    private String content;

    @Column(nullable = false)
    private Instant postDt;
    private Instant updateDt;

    @Column(nullable = false)
    @Setter
    private boolean isDeleted = false;


    @OneToMany(
            mappedBy = "parentPost",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            orphanRemoval = true
    )
    private List<Comment> comments;

    @OneToMany(
            mappedBy = "post",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Like> likes;

    @OneToMany(
            mappedBy = "post",
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            orphanRemoval = true
    )
    private List<Attachment> uploadedFiles;

    public NewPostResponseDTO toNewPostResponseDTO(){
        return NewPostResponseDTO.builder()
                .id(this.id)
                .author(this.author.getUsername())
                .content(this.content)
                .postDt(this.postDt)
                .updateDt(this.updateDt)
                .build();
    }

    public GetPostResponseDTO toGetPostResponseDTO(){
        return GetPostResponseDTO.builder()
                .id(this.id)
                .author(this.author.getUsername())
                .content(this.content)
                .postDt(this.postDt)
                .updateDt(this.updateDt)
                .commentCount(this.comments.size())
                .likeCount(this.likes.size())
                .build();
    }

    public EditPostResponseDTO toEditPostResponseDTO(){
        return EditPostResponseDTO.builder()
                .id(this.id)
                .author(this.author.getUsername())
                .content(this.content)
                .postDt(this.postDt)
                .updateDt(this.updateDt)
                .build();
    }

    public void addComment(Comment comment) {
        if(Objects.isNull(this.comments)) {
            this.comments = new ArrayList<>();
        }
        this.comments.add(comment);
    }
}

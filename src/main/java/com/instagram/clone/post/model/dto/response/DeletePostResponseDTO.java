package com.instagram.clone.post.model.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;

import java.time.Instant;

@Builder
@Getter
public class DeletePostResponseDTO {
    @JsonProperty("deleted_post_id")
    private long id;
}

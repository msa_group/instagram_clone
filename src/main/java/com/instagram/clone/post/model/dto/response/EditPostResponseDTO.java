package com.instagram.clone.post.model.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EditPostResponseDTO {

    @JsonProperty("post_id")
    private long id;

    private String author;
    private String content;

    @JsonProperty("post_dt")
    private Instant postDt;

    @JsonProperty("update_dt")
    private Instant updateDt;
}

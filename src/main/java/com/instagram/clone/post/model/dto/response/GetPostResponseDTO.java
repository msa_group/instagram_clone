package com.instagram.clone.post.model.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.instagram.clone.account.model.entity.Account;
import lombok.Builder;
import lombok.Getter;

import java.time.Instant;

@Builder
@Getter
public class GetPostResponseDTO {
    @JsonProperty("post_id")
    private long id;
    private String author;
    private String content;

    @JsonProperty("post_dt")
    private Instant postDt;

    @JsonProperty("update_dt")
    private Instant updateDt;

    @JsonProperty("comment_count")
    private int commentCount;

    @JsonProperty("like_count")
    private int likeCount;
}

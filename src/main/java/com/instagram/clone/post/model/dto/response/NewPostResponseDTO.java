package com.instagram.clone.post.model.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.attachment.model.entity.Attachment;
import com.instagram.clone.comment.model.entity.Comment;
import com.instagram.clone.like.model.entity.Like;
import lombok.Builder;
import lombok.Getter;

import java.time.Instant;
import java.util.List;

@Builder
@Getter
public class NewPostResponseDTO {
    @JsonProperty("post_id")
    private long id;

    private String author;
    private String content;

    @JsonProperty("post_dt")
    private Instant postDt;

    @JsonProperty("update_dt")
    private Instant updateDt;
}

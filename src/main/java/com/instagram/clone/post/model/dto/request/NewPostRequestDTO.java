package com.instagram.clone.post.model.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 신규 게시물 생성 신청 Request DTO
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NewPostRequestDTO {
    @JsonProperty("post_content")
    private String postContent;

    @JsonProperty("media_id")
    private Long mediaID;
}

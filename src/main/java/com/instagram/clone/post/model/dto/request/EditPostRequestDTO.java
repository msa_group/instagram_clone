package com.instagram.clone.post.model.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Post 수정 시 보내오는 JSON Body 형식
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class EditPostRequestDTO {
    @JsonProperty("post_content")
    private String postContent;
}

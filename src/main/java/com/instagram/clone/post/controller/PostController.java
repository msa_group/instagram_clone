package com.instagram.clone.post.controller;

import com.instagram.clone.common.api.ApiResult;
import com.instagram.clone.common.security.JwtAuthentication;
import com.instagram.clone.post.model.dto.request.EditPostRequestDTO;
import com.instagram.clone.post.model.dto.request.NewPostRequestDTO;
import com.instagram.clone.post.model.dto.response.DeletePostResponseDTO;
import com.instagram.clone.post.model.dto.response.EditPostResponseDTO;
import com.instagram.clone.post.model.dto.response.GetPostResponseDTO;
import com.instagram.clone.post.model.dto.response.NewPostResponseDTO;
import com.instagram.clone.post.service.PostService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import static com.instagram.clone.common.api.ApiResult.OK;

@RestController
@RequestMapping("/api/post")
public class PostController {
    private PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @PostMapping
    public ApiResult<NewPostResponseDTO> makeNewPost(@RequestBody NewPostRequestDTO newPost, @AuthenticationPrincipal JwtAuthentication auth){
        return OK(postService.makeNewPost(newPost, auth.getUserName()));
    }

    @GetMapping("/{postID}")
    public ApiResult<GetPostResponseDTO> getPost(@PathVariable Long postID){
        return OK(postService.getPost(postID));
    }

    @PutMapping("/{postID}")
    public ApiResult<EditPostResponseDTO> editPost(
            @AuthenticationPrincipal JwtAuthentication auth,
            @PathVariable Long postID,
            @RequestBody EditPostRequestDTO editPostPayload
            ){
        return OK(postService.editPost(postID, editPostPayload, auth));
    }

    @DeleteMapping("/{postID}")
    public ApiResult<DeletePostResponseDTO> deletePost(
            @AuthenticationPrincipal JwtAuthentication auth,
            @PathVariable Long postID
    ){
        return OK(postService.deletePost(postID, auth));
    }
}

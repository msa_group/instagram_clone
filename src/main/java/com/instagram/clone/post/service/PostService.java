package com.instagram.clone.post.service;

import com.instagram.clone.common.error.BadRequestException;
import com.instagram.clone.common.error.ForbiddenException;
import com.instagram.clone.common.error.NotFoundException;
import com.instagram.clone.common.security.JwtAuthentication;
import com.instagram.clone.post.model.dto.request.EditPostRequestDTO;
import com.instagram.clone.post.model.dto.request.NewPostRequestDTO;
import com.instagram.clone.post.model.dto.response.DeletePostResponseDTO;
import com.instagram.clone.post.model.dto.response.EditPostResponseDTO;
import com.instagram.clone.post.model.dto.response.GetPostResponseDTO;
import com.instagram.clone.post.model.dto.response.NewPostResponseDTO;

public interface PostService {

    NewPostResponseDTO makeNewPost(NewPostRequestDTO newPost, String username) throws NotFoundException, BadRequestException;
    GetPostResponseDTO getPost(Long postID) throws NotFoundException;
    EditPostResponseDTO editPost(
            Long postID,
            EditPostRequestDTO editPayload,
            JwtAuthentication auth
    ) throws NotFoundException, ForbiddenException, BadRequestException;

    DeletePostResponseDTO deletePost(Long postID, JwtAuthentication auth) throws ForbiddenException;
}

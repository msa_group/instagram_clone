package com.instagram.clone.post.service;

import com.amazonaws.util.CollectionUtils;
import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.account.repository.AccountRepository;
import com.instagram.clone.attachment.model.entity.Attachment;
import com.instagram.clone.attachment.repository.AttachmentRepository;
import com.instagram.clone.common.error.BadRequestException;
import com.instagram.clone.common.error.ForbiddenException;
import com.instagram.clone.common.error.NotFoundException;
import com.instagram.clone.common.security.JwtAuthentication;
import com.instagram.clone.post.model.dto.request.EditPostRequestDTO;
import com.instagram.clone.post.model.dto.request.NewPostRequestDTO;
import com.instagram.clone.post.model.dto.response.DeletePostResponseDTO;
import com.instagram.clone.post.model.dto.response.EditPostResponseDTO;
import com.instagram.clone.post.model.dto.response.GetPostResponseDTO;
import com.instagram.clone.post.model.dto.response.NewPostResponseDTO;
import com.instagram.clone.post.model.entity.Post;
import com.instagram.clone.post.repository.PostRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PostServiceImpl implements PostService {

    private PostRepository postRepository;
    private AccountRepository accountRepository;
    private AttachmentRepository attachmentRepository;

    public PostServiceImpl(PostRepository postRepository,
                           AccountRepository accountRepository,
                           AttachmentRepository attachmentRepository
    ) {
        this.postRepository = postRepository;
        this.accountRepository = accountRepository;
        this.attachmentRepository = attachmentRepository;
    }

    /**
     * 신규 포스트 생성
     * @param newPost
     * @param username
     * @return
     * @throws NotFoundException
     */
    public NewPostResponseDTO makeNewPost(NewPostRequestDTO newPost, String username)
            throws NotFoundException, BadRequestException{
        Account author = accountRepository.findByUsername(username);
        List<Attachment> postPhotos = findAttachments(newPost.getMediaID());

        if (CollectionUtils.isNullOrEmpty(postPhotos)) {
            throw new NotFoundException(Attachment.class, "No such media found");
        }

        Post postedPost = Post.builder()
                .author(author)
                .content(newPost.getPostContent())
                .postDt(Instant.now())
                .updateDt(Instant.now())
                .isDeleted(false)
                .uploadedFiles(postPhotos)
                .build();

        if (isPostContentBadInput(postedPost))
            throw new BadRequestException("Unacceptable post input");

        Post newlyCreatedPost = postRepository.save(postedPost);

        // TODO: OneToMany 관련 내용 은수님 피드백 접수
        for (Attachment photo: postPhotos){
            photo.setPost(postedPost);
            attachmentRepository.save(photo);
        }
        return newlyCreatedPost.toNewPostResponseDTO();
    }

    /**
     * 첨부파일 검색
     * @param attachmentID
     * @return
     */
    private List<Attachment> findAttachments(Long attachmentID){
        List<Attachment> attachmentList = new ArrayList<>();
        Optional<Attachment> postPhoto = attachmentRepository.findById(attachmentID);
        postPhoto.ifPresent(attachmentList::add);
        return attachmentList;
    }

    /**
     * Post를 Post ID 기준으로 찾아 준다
     * @param postID
     * @return
     * @throws NotFoundException
     */
    public GetPostResponseDTO getPost(Long postID) throws NotFoundException {
        return getExistingPost(postID).toGetPostResponseDTO();
    }

    public EditPostResponseDTO editPost(
            Long postID,
            EditPostRequestDTO editPayload,
            JwtAuthentication auth
    ) throws NotFoundException, ForbiddenException, BadRequestException {
        Post existingPost = getExistingPost(postID);
        if (!userOwnsPost(auth.getUserName(), existingPost))
            throw new ForbiddenException("Only post owners can delete posts");

        existingPost.setContent(editPayload.getPostContent());

        if (isPostContentBadInput(existingPost))
            throw new BadRequestException("Unacceptable post input");

        return postRepository.save(existingPost).toEditPostResponseDTO();
    }

    public DeletePostResponseDTO deletePost(Long postID, JwtAuthentication auth) throws ForbiddenException{
        Post existingPost = getExistingPost(postID);

        if (!userOwnsPost(auth.getUserName(), existingPost))
            throw new ForbiddenException("Only post owners can delete posts");
        existingPost.setDeleted(true);
        postRepository.save(existingPost);
        return DeletePostResponseDTO.builder().id(postID).build();
    }

    /**
     * 해당 포스트의 주인이 주어진 User인지 확인한다
     * @param post
     * @param username
     * @return
     */
    private boolean userOwnsPost(String username, Post post){
        return post.getAuthor().getUsername().equals(username);
    }

    private Post getExistingPost(Long postID) throws NotFoundException {
        Optional<Post> existingPostOptional = postRepository.findById(postID);
        existingPostOptional.orElseThrow(() -> new NotFoundException(Post.class, "No such post exists"));
        Post foundPost = existingPostOptional.get();
        if (foundPost.isDeleted())
            throw new NotFoundException(Post.class, "No such post exists");
        return foundPost;
    }

    private boolean isPostContentBadInput(Post post){
        return StringUtils.isBlank(post.getContent());
    }
}

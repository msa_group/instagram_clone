package com.instagram.clone.attachment.repository;

import com.instagram.clone.attachment.model.entity.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttachmentRepository extends JpaRepository<Attachment, Long> {
}

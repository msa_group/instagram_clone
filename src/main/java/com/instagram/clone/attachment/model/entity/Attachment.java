package com.instagram.clone.attachment.model.entity;

import com.instagram.clone.account.model.entity.AccountExtra;
import com.instagram.clone.post.model.entity.Post;
import lombok.*;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "attachment")
@SequenceGenerator(name = "attachment_id_sequence",
        sequenceName = "attachment_id_sequence",
        allocationSize = 25)
@Builder(toBuilder = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Attachment {

    public enum AttachmentType {PROFILE_PHOTO, POST_PHOTO}

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "attachment_id_sequence")
    private long id;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String uri;

    @Column(nullable = false)
    private Instant uploadDt;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private AttachmentType attachmentType;

    @OneToOne(mappedBy = "profilePhoto",fetch = FetchType.LAZY)
    private AccountExtra profile;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "post_id")
    private Post post;
}

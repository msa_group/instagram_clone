package com.instagram.clone.attachment.model.dto.response;

import lombok.*;


@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PostPhotoResponseDTO {
    private Long attachmentID;
}

package com.instagram.clone.attachment.service;

import com.instagram.clone.attachment.model.dto.response.PostPhotoResponseDTO;
import com.instagram.clone.common.error.InternalErrorException;
import com.instagram.clone.common.error.UnprocessableEntityException;
import org.springframework.web.multipart.MultipartFile;

public interface AttachmentService {
    PostPhotoResponseDTO savePostPhoto(MultipartFile uploadedFile) throws UnprocessableEntityException, InternalErrorException;
}

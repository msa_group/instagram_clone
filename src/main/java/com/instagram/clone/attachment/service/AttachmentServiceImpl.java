package com.instagram.clone.attachment.service;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.instagram.clone.attachment.model.dto.response.PostPhotoResponseDTO;
import com.instagram.clone.attachment.model.entity.Attachment;
import com.instagram.clone.attachment.repository.AttachmentRepository;
import com.instagram.clone.common.error.InternalErrorException;
import com.instagram.clone.common.error.UnprocessableEntityException;
import com.instagram.clone.common.type.ValidImgType;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.Instant;
import java.util.UUID;

import static com.instagram.clone.common.util.FileUtils.convertToFile;

@Service
public class AttachmentServiceImpl implements AttachmentService {

    private AmazonS3 s3Client;

    @Value("${aws.s3.photo-bucket}")
    private String photoBucketName;

    private final AttachmentRepository attachmentRepository;

    public AttachmentServiceImpl(AmazonS3 s3Client, AttachmentRepository attachmentRepository) {
        this.s3Client = s3Client;
        this.attachmentRepository = attachmentRepository;
    }

    /**
     * Post에 사용 될 사진을 S3에 저장 후 해당 정보를 DB에 저장한다
     * @param uploadedFile
     */
    public PostPhotoResponseDTO savePostPhoto(MultipartFile uploadedFile)
            throws UnprocessableEntityException, InternalErrorException {
        if(!isValidImageFile(uploadedFile)) throw new UnprocessableEntityException("Invalid image type"); // 400 Response
        URL s3ObjectURL;
        try {
            s3ObjectURL = uploadFileToS3(uploadedFile);
        }catch (IOException e){
            throw new UnprocessableEntityException("Unable to process given file");
        }catch (SdkClientException e){
            throw new InternalErrorException("Unknown error while saving image");
        }
        Attachment attachment = Attachment.builder()
                .uri(s3ObjectURL.toString())
                .uploadDt(Instant.now())
                .attachmentType(Attachment.AttachmentType.POST_PHOTO)
                .build();
        attachmentRepository.save(attachment);
        return PostPhotoResponseDTO
                .builder()
                .attachmentID(attachment.getId())
                .build();
    }

    /**
     * AWS S3 에 파일을 업로드 한다
     * @param uploadedFile 클라이언트가 업로드 한 파일
     * @return S3 Full URL
     * @throws IOException
     */
    private URL uploadFileToS3(MultipartFile uploadedFile)
            throws IOException, SdkClientException {
        String extension = FilenameUtils.getExtension(uploadedFile.getOriginalFilename());
        String photoName = makeNewFileName(extension);
        File photoFile = convertToFile(uploadedFile, photoName);
        String s3ObjName = "post-photo/" + photoName;
        s3Client.putObject(
                photoBucketName,
                s3ObjName,
                photoFile
        );
        photoFile.delete(); // 서버 로컬에 저장된 파일 삭제
        return s3Client.getUrl(photoBucketName, s3ObjName);
    }

    /**
     * 파일이 유효한 이미지인지 아닌지 확인한다. (BMP, GIF, JPG, PNG 중 하나)
     * 단순 확장자 체크가 아니라 파일의 헤더를 까서 확인하는 방식
     * https://stackoverflow.com/a/4169776/2850253
     * @param uploadedFile
     * @return
     */
    private boolean isValidImageFile(MultipartFile uploadedFile) {
        String fileExtension = FilenameUtils.getExtension(uploadedFile.getOriginalFilename());
        ValidImgType imgExtension = EnumUtils.getEnumIgnoreCase(ValidImgType.class, fileExtension); // Enum으로 안되면 null값이 반환된다
        if (imgExtension == null) return false;
        try (InputStream input = uploadedFile.getInputStream()) {
            try {
                ImageIO.read(input).toString();
                // It's an image (only BMP, GIF, JPG and PNG are recognized).
                return true;
            } catch (Exception e) {
                // It's not an image.
                return false;
            }
        }catch (IOException e){
            return false;
        }
    }


    private String makeNewFileName(String extension){
        return UUID.randomUUID().toString() + "." + extension;
    }

}

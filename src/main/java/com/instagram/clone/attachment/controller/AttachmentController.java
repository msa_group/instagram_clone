package com.instagram.clone.attachment.controller;

import com.instagram.clone.attachment.model.dto.response.PostPhotoResponseDTO;
import com.instagram.clone.attachment.service.AttachmentService;
import com.instagram.clone.common.api.ApiResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import static com.instagram.clone.common.api.ApiResult.OK;


@RestController
@RequestMapping("/api/attachment")
public class AttachmentController {

    private final AttachmentService attachmentService;

    public AttachmentController(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

    /**
     * Post 에 사용 될 사진 업로드
     * @param postPhoto
     * @return
     */
    @PostMapping("/post-photo")
    public ApiResult<PostPhotoResponseDTO> newPostPhotoAttachment(@RequestParam("file") MultipartFile postPhoto){
        return OK(attachmentService.savePostPhoto(postPhoto));
    }
}

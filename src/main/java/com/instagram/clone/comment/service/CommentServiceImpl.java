package com.instagram.clone.comment.service;

import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.account.repository.AccountRepository;
import com.instagram.clone.comment.model.dto.request.CreateCommentRequestDTO;
import com.instagram.clone.comment.model.dto.request.ModifyCommentRequestDTO;
import com.instagram.clone.comment.model.dto.response.CommentResponseDTO;
import com.instagram.clone.comment.model.entity.Comment;
import com.instagram.clone.comment.repository.CommentRepository;
import com.instagram.clone.common.error.NotFoundException;
import com.instagram.clone.post.model.entity.Post;
import com.instagram.clone.post.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Slf4j
@RequiredArgsConstructor
@Service
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;

    private final AccountRepository accountRepository;

    private final PostRepository postRepository;

    @Override
    public CommentResponseDTO createComment(String userName, CreateCommentRequestDTO commentRequestDTO) {
        Account account = accountRepository.findByUsername(userName);
        Post post = postRepository.findById(commentRequestDTO.getPostId())
                .orElseThrow(() -> new NotFoundException(Post.class, commentRequestDTO.getPostId(), "해당 post가 존재하지 않습니다."));
        Comment comment = Comment.builder()
                .author(account)
                .parentPost(post)
                .content(commentRequestDTO.getCommentContent())
                .postDt(Instant.now())
                .build();
        account.addComment(comment);
        post.addComment(comment);
        Comment saveComment = commentRepository.save(comment);
        return CommentResponseDTO.of(saveComment);
    }

    @Override
    public CommentResponseDTO getComment(long id) {
        return CommentResponseDTO.of(commentRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(Comment.class, id, "해당 comment가 존재하지 않습니다.")));
    }

    @Override
    public CommentResponseDTO modifyComment(long id, ModifyCommentRequestDTO modifyCommentRequestDTO) {
        return CommentResponseDTO.of(commentRepository.findById(id)
                .map(c -> {

                    c.setContent(modifyCommentRequestDTO.getCommentContent());
                    return commentRepository.save(c);
                })
                .orElseThrow(() -> new NotFoundException(Comment.class, id, "해당 comment가 존재하지 않습니다.")));
    }

    @Override
    public void destroyComment(long id) {
        commentRepository.delete(commentRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(Comment.class, id, "해당 comment가 존재하지 않습니다.")));
    }
}

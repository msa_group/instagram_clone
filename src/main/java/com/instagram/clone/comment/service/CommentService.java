package com.instagram.clone.comment.service;

import com.instagram.clone.comment.model.dto.request.CreateCommentRequestDTO;
import com.instagram.clone.comment.model.dto.request.ModifyCommentRequestDTO;
import com.instagram.clone.comment.model.dto.response.CommentResponseDTO;

public interface CommentService {
    CommentResponseDTO createComment(String userName, CreateCommentRequestDTO commentRequestDTO);

    CommentResponseDTO getComment(long id);

    CommentResponseDTO modifyComment(long id, ModifyCommentRequestDTO modifyCommentRequestDTO);

    void destroyComment(long id);
}

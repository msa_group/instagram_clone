package com.instagram.clone.comment.model.dto.request;

import lombok.*;

/**
 * 댓글 생성 Request DTO
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ToString
public class CreateCommentRequestDTO {

    private long postId;

    private String commentContent;
}

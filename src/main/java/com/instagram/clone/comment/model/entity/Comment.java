package com.instagram.clone.comment.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.like.model.entity.Like;
import com.instagram.clone.post.model.entity.Post;
import lombok.*;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Getter
//@ToString(exclude = {"author", "parentPost", "likes"})
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="comment")
@SequenceGenerator(name="comment_id_sequence",
        sequenceName="comment_id_sequence",
        allocationSize = 25)
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "comment_id_sequence")
    private long id;

    @ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", nullable = false)
    private Account author;

    @ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "post_id", nullable = false)
    private Post parentPost;

    @Column(length=300, nullable = false) @Setter
    private String content;

    @Column(nullable = false)
    private Instant postDt;

    @Column(nullable = false)
    private boolean isDeleted = false;

    @OneToMany(
            mappedBy = "comment",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            orphanRemoval = true
    )
    private List<Like> likes;
}

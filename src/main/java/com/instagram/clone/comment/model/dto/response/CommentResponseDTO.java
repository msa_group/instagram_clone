package com.instagram.clone.comment.model.dto.response;

import com.instagram.clone.account.model.dto.response.AccountResponseDTO;
import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.comment.model.entity.Comment;
import com.instagram.clone.like.model.entity.Like;
import com.instagram.clone.post.model.entity.Post;
import lombok.Builder;
import lombok.Getter;

import java.time.Instant;
import java.util.List;

/**
 * 댓글 응답 Response DTO
 */
@Getter
@Builder
public class CommentResponseDTO {

    private long id;

    private long authorId;

    private long parentPostId;

    private String content;

    private Instant postDt;

    private boolean isDeleted;

    private List<Like> likes;

    public static CommentResponseDTO of(Comment comment) {
        return CommentResponseDTO.builder()
                .id(comment.getId())
                .authorId(comment.getAuthor().getId())
                .parentPostId(comment.getParentPost().getId())
                .content(comment.getContent())
                .postDt(comment.getPostDt())
                .isDeleted(comment.isDeleted())
                .likes(comment.getLikes())
                .build();
    }
}

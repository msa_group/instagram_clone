package com.instagram.clone.comment.model.dto.request;

import lombok.*;

/**
 * 댓글 수정 Request DTO
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@ToString
public class ModifyCommentRequestDTO {

    private String commentContent;
}

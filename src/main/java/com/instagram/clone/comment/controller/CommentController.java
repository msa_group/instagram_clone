package com.instagram.clone.comment.controller;

import com.instagram.clone.comment.model.dto.request.CreateCommentRequestDTO;
import com.instagram.clone.comment.model.dto.request.ModifyCommentRequestDTO;
import com.instagram.clone.comment.model.dto.response.CommentResponseDTO;
import com.instagram.clone.comment.service.CommentService;
import com.instagram.clone.common.api.ApiResult;
import com.instagram.clone.common.security.JwtAuthentication;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import static com.instagram.clone.common.api.ApiResult.OK;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/comments")
public class CommentController {

    private final CommentService commentService;

    @PostMapping
    public ResponseEntity<ApiResult<CommentResponseDTO>> createComment(
            @AuthenticationPrincipal JwtAuthentication jwtAuthentication,
            @RequestBody CreateCommentRequestDTO commentRequestDTO) throws URISyntaxException {
        CommentResponseDTO commentResponseDTO = commentService
                .createComment(jwtAuthentication.getUserName(), commentRequestDTO);
        return ResponseEntity.created(new URI("/api/comments/" + commentResponseDTO.getId()))
                .body(OK(commentResponseDTO));
    }

    @GetMapping("{id}")
    public ResponseEntity<ApiResult<CommentResponseDTO>> getComment(@PathVariable long id) {
        return ResponseEntity.ok().body(OK(commentService.getComment(id)));
    }

    @PutMapping("{id}")
    public ResponseEntity<ApiResult<CommentResponseDTO>> modifyComment(@PathVariable long id, @RequestBody ModifyCommentRequestDTO modifyCommentRequestDTO) {
        return ResponseEntity.ok(OK(commentService.modifyComment(id, modifyCommentRequestDTO)));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> destroyComment(@PathVariable long id) {
        commentService.destroyComment(id);
        return ResponseEntity.noContent().build();
    }
}

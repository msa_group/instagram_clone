package com.instagram.clone.account.service;

import com.instagram.clone.account.model.dto.request.ProfileRequestDTO;
import com.instagram.clone.account.model.dto.request.SignUpRequestDTO;
import com.instagram.clone.account.model.dto.request.follow.UnFollowRequest;
import com.instagram.clone.account.model.dto.request.follow.FollowRequest;
import com.instagram.clone.account.model.dto.response.AccountResponseDTO;
import com.instagram.clone.account.model.dto.response.follow.FollowResponseDTO;
import com.instagram.clone.account.model.dto.response.LoginResponseDTO;
import com.instagram.clone.account.model.dto.response.ProfileResponseDTO;
import com.instagram.clone.account.model.dto.response.follow.UnFollowResponseDTO;

import java.util.List;

public interface AccountService {

    LoginResponseDTO signIn(String principal, String credentials);

    AccountResponseDTO signUp(SignUpRequestDTO userRequestingSignUp);

    FollowResponseDTO follow(FollowRequest followRequest);

    UnFollowResponseDTO unFollow(UnFollowRequest unFollowRequest);

    List<AccountResponseDTO> getFollowers(FollowRequest followRequest);

    List<AccountResponseDTO> getFollowings(FollowRequest followRequest);

    boolean isNotExists(long principalId);

    ProfileResponseDTO getProfile(long accountId);

    ProfileResponseDTO setProfile(ProfileRequestDTO profileRequestDTO);

    String removeAccount(ProfileRequestDTO profileRequestDTO);
}
package com.instagram.clone.account.service;

import com.instagram.clone.account.model.dto.request.ProfileRequestDTO;
import com.instagram.clone.account.model.dto.request.SignUpRequestDTO;
import com.instagram.clone.account.model.dto.request.follow.FollowRequest;
import com.instagram.clone.account.model.dto.request.follow.UnFollowRequest;
import com.instagram.clone.account.model.dto.response.AccountResponseDTO;
import com.instagram.clone.account.model.dto.response.LoginResponseDTO;
import com.instagram.clone.account.model.dto.response.ProfileResponseDTO;
import com.instagram.clone.account.model.dto.response.follow.FollowResponseDTO;
import com.instagram.clone.account.model.dto.response.follow.UnFollowResponseDTO;
import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.account.model.entity.AccountExtra;
import com.instagram.clone.account.model.entity.FollowRel;
import com.instagram.clone.account.model.entity.FollowRelId;
import com.instagram.clone.account.repository.AccountExtraRepository;
import com.instagram.clone.account.repository.AccountRepository;
import com.instagram.clone.account.repository.FollowRepository;
import com.instagram.clone.common.error.AlreadyExistsException;
import com.instagram.clone.common.error.NotFoundException;
import com.instagram.clone.common.error.UnauthorizedException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {


    private final AccountRepository accountRepository;

    private final PasswordEncoder passwordEncoder;

    private final AccountExtraRepository accountExtraRepository;

    private final FollowRepository followRepository;


    @Override
    public LoginResponseDTO signIn(String principal, String credentials){
        Account account = Optional.ofNullable(accountRepository.findByUsername(principal)).orElseThrow(() -> {throw new NotFoundException(Account.class, principal);});
        if (account.isWrongPassword(credentials, passwordEncoder)) {
            throw new UnauthorizedException("Bad Credentials");
        }

        return LoginResponseDTO.of(account);
    }

    @Override
    public AccountResponseDTO signUp(SignUpRequestDTO userRequestingSignUp) {
        Account newUser = Account.builder()
                .username(userRequestingSignUp.getUsername())
                .password(passwordEncoder.encode(userRequestingSignUp.getPassword()))
                .nickname(userRequestingSignUp.getNickname())
                .isActive(true)
                .build();

        return AccountResponseDTO.of(accountRepository.save(newUser));
    }

    @Override
    public FollowResponseDTO follow(FollowRequest followRequest) {
        Account principal = findById(followRequest.getJwtAuthentication().getId().value());
        Account targetAccount = findById(followRequest.getTargetAccountId().value());

        if (followRepository.existsById(FollowRelId.of(followRequest))) {
            throw new AlreadyExistsException(FollowRel.class, principal.getNickname(), " follow ", targetAccount.getNickname());
        }

        FollowRel followRel = FollowRel.follow(principal, targetAccount);

        return FollowResponseDTO.of(followRepository.save(followRel));
    }

    @Override
    public UnFollowResponseDTO unFollow(UnFollowRequest unFollowRequest) {
        Account principal = findById(unFollowRequest.getJwtAuthentication().getId().value());
        Account targetAccount = findById(unFollowRequest.getTargetAccountId().value());

        if ( !followRepository.existsById(FollowRelId.of(unFollowRequest)) ) {
            throw new NotFoundException(FollowRel.class, "There is no history that ", principal.getNickname() + " follow " + targetAccount.getNickname());
        }

        FollowRel followRel = FollowRel.unFollow(principal, targetAccount);

        followRepository.delete(followRel);
        return UnFollowResponseDTO.of(followRel);
    }

    @Override
    public List<AccountResponseDTO> getFollowers(FollowRequest followRequest) {
        return followRepository.getFollowers(followRequest.getTargetAccountId().value())
                .stream()
                .map(AccountResponseDTO::of)
                .collect(Collectors.toList());
    }

    @Override
    public List<AccountResponseDTO> getFollowings(FollowRequest followRequest) {
        return followRepository.getFollowings(followRequest.getTargetAccountId().value())
                .stream()
                .map(AccountResponseDTO::of)
                .collect(Collectors.toList());
    }

    @Override
    public boolean isNotExists(long principalId) {
        return !accountRepository.existsById(principalId);
    }

    private Account findById(long accountId) {
        return accountRepository.findById(accountId)
                .orElseThrow(() -> {throw new NotFoundException(Account.class, accountId);});
    }

    @Override
    public ProfileResponseDTO getProfile(long accountId) {
        Account account = accountRepository.findById(accountId).orElseThrow(() -> {
            throw new NotFoundException(Account.class, accountId);
        });
        return ProfileResponseDTO.of(account);
    }

    @Override
    public ProfileResponseDTO setProfile(ProfileRequestDTO profileRequestDTO) {
        long id = profileRequestDTO.getId();
        Account account = accountRepository.findById(id).orElseThrow(() -> {
            throw new NotFoundException(Account.class, id);
        });

        account.setNickname(profileRequestDTO.getNickname());
        account.setActive(profileRequestDTO.isActive());

        if (!isMatchPassword(profileRequestDTO, account)) {
            account.setPassword(passwordEncoder.encode(profileRequestDTO.getPassword()));
        }

        AccountExtra accountExtra = Optional.ofNullable(account.getAccountExtra())
                                            .orElseGet(AccountExtra::new);

        AccountExtra.setAccountExtraInfo(accountExtra, account, profileRequestDTO);
        accountExtraRepository.save(accountExtra);
        return ProfileResponseDTO.of(account);
    }

    private boolean isMatchPassword(ProfileRequestDTO profileRequestDTO, Account account) {
        return passwordEncoder.matches(profileRequestDTO.getPassword(), account.getPassword());
    }

    @Override
    public String removeAccount(ProfileRequestDTO profileRequestDTO) {
        long id = profileRequestDTO.getId();
        Account account = accountRepository.findById(id).orElseThrow(() -> {
            throw new NotFoundException(Account.class, id);
        });
        account.setActive(false);
        accountRepository.save(account);

        return "The account has been locked out";
    }
}

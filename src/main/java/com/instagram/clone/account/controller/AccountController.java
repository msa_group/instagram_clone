package com.instagram.clone.account.controller;

import com.instagram.clone.account.model.dto.request.ProfileRequestDTO;
import com.instagram.clone.account.model.dto.request.SignUpRequestDTO;
import com.instagram.clone.account.model.dto.request.follow.FollowRequest;
import com.instagram.clone.account.model.dto.request.follow.UnFollowRequest;
import com.instagram.clone.account.model.dto.response.AccountResponseDTO;
import com.instagram.clone.account.model.dto.response.ProfileResponseDTO;
import com.instagram.clone.account.model.dto.response.follow.FollowResponseDTO;
import com.instagram.clone.account.model.dto.response.follow.UnFollowResponseDTO;
import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.account.service.AccountService;
import com.instagram.clone.common.api.ApiResult;
import com.instagram.clone.common.common.AccountContext;
import com.instagram.clone.common.common.Id;
import com.instagram.clone.common.security.JwtAuthentication;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.instagram.clone.common.api.ApiResult.OK;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/account")
public class AccountController {

    private final AccountService accountService;

    @PostMapping
    public ResponseEntity<ApiResult<AccountResponseDTO>> signUpAccount(@RequestBody SignUpRequestDTO userRequestingSignUp){
        AccountContext.setAccountName(userRequestingSignUp.getUsername());
        ResponseEntity<ApiResult<AccountResponseDTO>> response = ResponseEntity.ok().body(OK(accountService.signUp(userRequestingSignUp)));
        AccountContext.setAccountName(null);
        return response;
    }

    @PostMapping("/follow/{accountId}")
    public ResponseEntity<ApiResult<FollowResponseDTO>> follow(
            @AuthenticationPrincipal JwtAuthentication jwtAuthentication,
            @PathVariable long accountId) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(OK(accountService.follow(FollowRequest.create(jwtAuthentication, Id.of(Account.class, accountId)))));

    }

    @DeleteMapping("/follow/{accountId}")
    public ResponseEntity<ApiResult<UnFollowResponseDTO>> unFollow(
            @AuthenticationPrincipal JwtAuthentication jwtAuthentication,
            @PathVariable long accountId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(OK(accountService.unFollow(UnFollowRequest.create(jwtAuthentication, Id.of(Account.class, accountId)))));
    }

    @PostMapping("/followers/{accountId}")
    public ResponseEntity<ApiResult<List<AccountResponseDTO>>> getFollowers(
            @AuthenticationPrincipal JwtAuthentication jwtAuthentication,
            @PathVariable long accountId) {
        return ResponseEntity.status(HttpStatus.OK).body(OK(
                accountService.getFollowers(FollowRequest.create(jwtAuthentication, Id.of(Account.class, accountId)))));
    }

    @PostMapping("/followings/{accountId}")
    public ResponseEntity<ApiResult<List<AccountResponseDTO>>> getFollowings(
            @AuthenticationPrincipal JwtAuthentication jwtAuthentication,
            @PathVariable long accountId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(OK(accountService.getFollowings(FollowRequest.create(jwtAuthentication, Id.of(Account.class, accountId)))));
    }

    @GetMapping("{accountId}")
    public ResponseEntity<ApiResult<ProfileResponseDTO>> getProfile(@PathVariable long accountId){
        return ResponseEntity.ok().body(OK(accountService.getProfile(accountId)));
    }

    @PostMapping("{accountId}")
    public ResponseEntity<ApiResult<ProfileResponseDTO>> setProfile(@RequestBody ProfileRequestDTO profileRequestDTO, @PathVariable long accountId){
        profileRequestDTO.setId(accountId);
        return ResponseEntity.ok().body(OK(accountService.setProfile(profileRequestDTO)));
    }

    @DeleteMapping("{accountId}")
    public ApiResult<String> removeAccount(@RequestBody ProfileRequestDTO profileRequestDTO, @PathVariable long accountId){
        profileRequestDTO.setId(accountId);
        return OK(accountService.removeAccount(profileRequestDTO));
    }
}

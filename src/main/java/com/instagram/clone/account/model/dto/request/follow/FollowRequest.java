package com.instagram.clone.account.model.dto.request.follow;

import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.common.common.Id;
import com.instagram.clone.common.security.JwtAuthentication;
import lombok.Getter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Getter
public final class FollowRequest {
    private JwtAuthentication jwtAuthentication;
    private Id<Account, Long> targetAccountId;

    private FollowRequest() {}

    public static FollowRequest create(JwtAuthentication jwtAuthentication, Id<Account, Long> targetAccountId) {
        FollowRequest request = new FollowRequest();
        request.jwtAuthentication = jwtAuthentication;
        request.targetAccountId = targetAccountId;
        return request;
    }

    @Override
    public String toString() {
        return new ReflectionToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .toString();
    }
}

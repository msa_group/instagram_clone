package com.instagram.clone.account.model.entity;

import com.instagram.clone.account.model.dto.request.follow.UnFollowRequest;
import com.instagram.clone.account.model.dto.request.follow.FollowRequest;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class FollowRelId implements Serializable {

    private long follower;
    private long following;

    public static FollowRelId of(FollowRequest followRequest) {
        FollowRelId id = new FollowRelId();
        id.follower = followRequest.getJwtAuthentication().id.value();
        id.following = followRequest.getTargetAccountId().value();
        return id;
    }

    public static FollowRelId of(UnFollowRequest unFollowRequest) {
        FollowRelId id = new FollowRelId();
        id.follower = unFollowRequest.getJwtAuthentication().id.value();
        id.following = unFollowRequest.getTargetAccountId().value();
        return id;
    }
}

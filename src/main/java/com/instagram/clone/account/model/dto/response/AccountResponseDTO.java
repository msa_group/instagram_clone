package com.instagram.clone.account.model.dto.response;

import com.instagram.clone.account.model.entity.Account;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AccountResponseDTO {
    private long id;
    private String username;
    private String nickname;

    public AccountResponseDTO(){}

    public static AccountResponseDTO of(Account account) {
        AccountResponseDTO dto = new AccountResponseDTO();
        dto.id = account.getId();
        dto.username = account.getUsername();
        dto.nickname = account.getNickname();
        return dto;
    }
}

package com.instagram.clone.account.model.entity;

import com.instagram.clone.account.model.dto.request.ProfileRequestDTO;
import com.instagram.clone.attachment.model.entity.Attachment;
import lombok.*;
import org.hibernate.engine.spi.PersistentAttributeInterceptable;
import org.hibernate.engine.spi.PersistentAttributeInterceptor;

import javax.persistence.*;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountExtra extends AuditingBaseEntity implements PersistentAttributeInterceptable {
    public enum Gender {MALE, FEMALE}

    @Id @GeneratedValue
    @Column(name = "ACCOUNT_EXTRA_ID")
    private long id;

    @OneToOne(
            optional = false,
            fetch = FetchType.LAZY,
            targetEntity = Account.class
    )
    @JoinColumn(name = "ACCOUNT_ID")
    private Account account;

    @Column(columnDefinition = "TEXT")
    @Getter @Setter
    private String website;

    @Column(columnDefinition = "TEXT")
    @Getter @Setter
    private String intro;

    @Getter @Setter
    private String email;

    @Column(length = 20)
    @Getter @Setter
    private String telephone;

    @Enumerated(EnumType.STRING)
    @Getter @Setter
    private Gender gender;

    @OneToOne(
            fetch = FetchType.LAZY
    )
    private Attachment profilePhoto;

    @Transient
    private PersistentAttributeInterceptor interceptor;

    @Override
    public PersistentAttributeInterceptor $$_hibernate_getInterceptor() {
        return this.interceptor;
    }

    @Override
    public void $$_hibernate_setInterceptor(PersistentAttributeInterceptor interceptor) {
        this.interceptor = interceptor;
    }
    public Account getAccount(){
        if(null != interceptor){
            this.account = (Account) interceptor.readObject(this,"account",account);
        }
        return this.account;
    }

    public void setAccount(Account account){
        if(null != interceptor){
            this.account = (Account) interceptor.writeObject(this,"account", this.account,account);
        } else{
            this.account = account;
        }
        if(this.account.getAccountExtra() != this){
            this.account.setAccountExtra(this);
        }
    }

    public static void setAccountExtraInfo(AccountExtra accountExtra, Account account, ProfileRequestDTO requestDTO){
        accountExtra.setAccount(account);
        accountExtra.setIntro(requestDTO.getIntro());
        accountExtra.setWebsite(requestDTO.getWebsite());
        accountExtra.setEmail(requestDTO.getEmail());
        accountExtra.setTelephone(requestDTO.getTelephone());
        accountExtra.setGender(AccountExtra.Gender.valueOf(requestDTO.getGender()));
    }

}

package com.instagram.clone.account.model.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Eunsoo Kim
 * @since 2019-09-29
 */

@Getter
@Setter
@ToString
public class ProfileRequestDTO {
    private long id;
    private String nickname;
    private String intro;
    private String website;
    private String email;
    private String telephone;
    private String gender;
    private String password;
    @JsonProperty("isActive")
    private boolean isActive;
}

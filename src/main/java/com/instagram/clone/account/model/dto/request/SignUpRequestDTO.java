package com.instagram.clone.account.model.dto.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SignUpRequestDTO {
    private String username;
    private String password;
    private String nickname;
}

package com.instagram.clone.account.model.dto.request.follow;

import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.common.common.Id;
import com.instagram.clone.common.security.JwtAuthentication;
import lombok.Getter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Getter
public final class UnFollowRequest {
    private JwtAuthentication jwtAuthentication;
    private Id<Account, Long> targetAccountId;

    private UnFollowRequest() {}

    public static UnFollowRequest create(JwtAuthentication jwtAuthentication, Id<Account, Long> accountId){
        UnFollowRequest request = new UnFollowRequest();
        request.jwtAuthentication = jwtAuthentication;
        request.targetAccountId = accountId;
        return request;
    }

    @Override
    public String toString() {
        return new ReflectionToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .toString();
    }
}

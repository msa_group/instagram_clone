package com.instagram.clone.account.model.dto.response;

import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.common.security.JWT;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginResponseDTO {
    private long id;
    private String username;
    private String nickname;

    private LoginResponseDTO(){}

    public static LoginResponseDTO of(Account account) {
        LoginResponseDTO dto = new LoginResponseDTO();
        dto.id = account.getId();
        dto.username = account.getUsername();
        dto.nickname = account.getNickname();
        return dto;
    }

    public String newApiToken(JWT jwt, String userAgent, String[] roles) {
        JWT.Claims claims = JWT.Claims.of(id, username, nickname, userAgent, roles);
        return jwt.newToken(claims);
    }
}

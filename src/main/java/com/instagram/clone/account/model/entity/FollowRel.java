package com.instagram.clone.account.model.entity;

import lombok.Getter;

import javax.persistence.*;

@Getter
@Entity
@Table(name="follow_rel")
@IdClass(FollowRelId.class)
public class FollowRel {

    @Id
    @ManyToOne(fetch=FetchType.LAZY)
    private Account follower;

    @Id
    @ManyToOne(fetch=FetchType.LAZY)
    private Account following;

    public static FollowRel follow(Account principal, Account target) {
        FollowRel followRel = new FollowRel();
        followRel.follower = principal;
        followRel.following = target;
        return followRel;
    }

    public static FollowRel unFollow(Account principal, Account target) {
        FollowRel followRel = new FollowRel();
        followRel.follower = principal;
        followRel.following = target;
        return followRel;
    }

}

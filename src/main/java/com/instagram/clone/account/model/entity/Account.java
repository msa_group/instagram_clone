package com.instagram.clone.account.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.instagram.clone.comment.model.entity.Comment;
import com.instagram.clone.common.converter.BooleanToYNConverter;
import com.instagram.clone.like.model.entity.Like;
import com.instagram.clone.post.model.entity.Post;
import lombok.*;
import org.hibernate.annotations.LazyGroup;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.hibernate.engine.spi.PersistentAttributeInterceptable;
import org.hibernate.engine.spi.PersistentAttributeInterceptor;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="account")
@SequenceGenerator(name="account_id_sequence",
        sequenceName="account_id_sequence",
        allocationSize = 25)
public class Account extends AuditingBaseEntity implements PersistentAttributeInterceptable {
    @Id
    @Getter
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_id_sequence")
    private long id;

    @Getter @Setter
    @Column(nullable = false, length = 50, unique = true)
    private String username;

    @Column(nullable = false, columnDefinition = "TEXT")
    @Getter @Setter
    private String password;

    @Getter @Setter
    @Column(length = 50, unique = true)
    private String nickname;

    @Column(nullable = false) @Getter @Setter
    @Convert(converter = BooleanToYNConverter.class)
    private boolean isActive = true;

    @OneToOne(
            mappedBy = "account",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            optional = false
    )
    @LazyToOne(LazyToOneOption.NO_PROXY)
    @LazyGroup("accountExtra")
    private AccountExtra accountExtra;

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Post> authoredPosts;

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Comment> authoredComments;

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Like> likes;

    @Getter
    @OneToMany(mappedBy = "follower", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<FollowRel> followers;

    @Getter
    @OneToMany(mappedBy = "following", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<FollowRel> following;

    @Transient
    private PersistentAttributeInterceptor interceptor;

    public boolean isWrongPassword(String credentials, PasswordEncoder passwordEncoder){
        return !passwordEncoder.matches(credentials, password);
    }

    public AccountExtra getAccountExtra(){
        if(null != interceptor){
            this.accountExtra = (AccountExtra) interceptor.readObject(this,"accountExtra",accountExtra);
        }
        return this.accountExtra;
    }

    public void setAccountExtra(AccountExtra accountExtra){
        if(null != interceptor){
            this.accountExtra = (AccountExtra) interceptor.writeObject(this,"accountExtra", this.accountExtra,accountExtra);
        } else{
            this.accountExtra = accountExtra;
        }
        if(this.accountExtra.getAccount() != this){
            this.accountExtra.setAccount(this);
        }
    }

    public void addComment(Comment comment) {
        if(Objects.isNull(this.authoredComments)) {
            this.authoredComments = new ArrayList<>();
        }
        this.authoredComments.add(comment);
    }

    @Override
    public PersistentAttributeInterceptor $$_hibernate_getInterceptor() {
        return interceptor;
    }

    @Override
    public void $$_hibernate_setInterceptor(PersistentAttributeInterceptor interceptor) {
        this.interceptor = interceptor;
    }
}

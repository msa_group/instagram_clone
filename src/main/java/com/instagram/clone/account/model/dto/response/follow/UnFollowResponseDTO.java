package com.instagram.clone.account.model.dto.response.follow;

import com.instagram.clone.account.model.entity.FollowRel;
import lombok.Getter;

@Getter
public final class UnFollowResponseDTO {
    public static UnFollowResponseDTO NONE = new UnFollowResponseDTO();

    private String message;

    public static UnFollowResponseDTO of(FollowRel followRel) {
        UnFollowResponseDTO dto = new UnFollowResponseDTO();
        StringBuilder messageBuilder = new StringBuilder("From now, ")
                .append(followRel.getFollower().getId())
                .append(" : ")
                .append(followRel.getFollower().getNickname())
                .append(" doesn't follow ")
                .append(followRel.getFollowing().getId())
                .append(" : ")
                .append(followRel.getFollowing().getNickname());
        dto.message = messageBuilder.toString();
        return dto;
    }
}

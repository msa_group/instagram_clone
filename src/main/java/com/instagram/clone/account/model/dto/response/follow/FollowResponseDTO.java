package com.instagram.clone.account.model.dto.response.follow;

import com.instagram.clone.account.model.dto.response.AccountResponseDTO;
import com.instagram.clone.account.model.entity.FollowRel;
import lombok.Getter;

@Getter
public final class FollowResponseDTO {

    private AccountResponseDTO follower;
    private AccountResponseDTO following;

    public static FollowResponseDTO of(FollowRel followRel) {
        FollowResponseDTO dto = new FollowResponseDTO();
        dto.follower = AccountResponseDTO.of(followRel.getFollower());
        dto.following = AccountResponseDTO.of(followRel.getFollowing());
        return dto;
    }
}

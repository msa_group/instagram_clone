package com.instagram.clone.account.model.dto.request;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Eunsoo Kim
 * @since 2019-09-22
 */

@Getter
@Setter
public class SignInRequestDTO {
    private String username;
    private String password;
}

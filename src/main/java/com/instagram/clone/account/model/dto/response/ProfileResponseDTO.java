package com.instagram.clone.account.model.dto.response;

import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.account.model.entity.AccountExtra;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Eunsoo Kim
 * @since 2019-09-29
 */

@Getter
@Setter
public class ProfileResponseDTO {
    private long id;
    private String username;
    private String nickname;
    private String intro;
    private String website;
    private String email;
    private String telephone;
    private String gender;
    private boolean isActive;

    public static ProfileResponseDTO of(Account account) {
        ProfileResponseDTO dto = new ProfileResponseDTO();
        final AccountExtra accountExtra = account.getAccountExtra();
        dto.id = account.getId();
        dto.username = account.getUsername();
        dto.nickname = account.getNickname();
        dto.intro = accountExtra.getIntro();
        dto.website = accountExtra.getWebsite();
        dto.email = accountExtra.getEmail();
        dto.telephone = accountExtra.getTelephone();
        dto.gender = accountExtra.getGender().toString();
        dto.isActive = account.isActive();
        return dto;
    }
}

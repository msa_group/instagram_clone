package com.instagram.clone.account.repository;

import com.instagram.clone.account.model.entity.AccountExtra;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Eunsoo Kim
 * @since 2019-09-29
 */
public interface AccountExtraRepository extends JpaRepository<AccountExtra, Long> {
    AccountExtra findByAccount_Id(long account_id);
}

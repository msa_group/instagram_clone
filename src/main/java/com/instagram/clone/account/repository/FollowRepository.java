package com.instagram.clone.account.repository;

import com.instagram.clone.account.model.entity.Account;
import com.instagram.clone.account.model.entity.FollowRel;
import com.instagram.clone.account.model.entity.FollowRelId;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FollowRepository extends JpaRepository<FollowRel, FollowRelId> {

    @Query("SELECT a FROM Account a INNER JOIN a.followers WHERE following_id = :target_id")
    List<Account> getFollowers(@Param("target_id") long targetId);

    @Query("SELECT a FROM Account a INNER JOIN a.following WHERE follower_id = :target_id")
    List<Account> getFollowings(@Param("target_id") long targetId);
}

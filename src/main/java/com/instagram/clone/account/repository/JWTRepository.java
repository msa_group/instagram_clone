package com.instagram.clone.account.repository;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Eunsoo Kim
 * @since 2019-09-29
 */
@Repository
public class JWTRepository  {
    private final String KEY = "apiToken";
    private final HashOperations hashOperations;

    public JWTRepository(RedisTemplate redisTemplate) {
        this.hashOperations = redisTemplate.opsForHash();
    }

    public void save(String username, String jwtToken){
        hashOperations.put(KEY, username, jwtToken );
    }

    public String findByUserKey(String userKey) {
        String value = (String)hashOperations.get(KEY, userKey);
        checkNotNull(value,"JWT does not exist in repository. userkey(" + userKey + ")");
        return value;
    }

    public long delete(String username){
        return hashOperations.delete(KEY, username);
    }
}

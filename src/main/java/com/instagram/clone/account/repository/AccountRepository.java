package com.instagram.clone.account.repository;

import com.instagram.clone.account.model.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AccountRepository extends JpaRepository<Account, Long> {
    Account findByUsername(String username);
}
